package com.secata.tools.rest.testing;

import com.google.errorprone.annotations.CanIgnoreReturnValue;
import com.secata.tools.rest.ClassBinder;
import com.secata.tools.rest.ClassBinder.RegisteredBinding;
import com.secata.tools.thread.ShutdownHook;
import java.util.List;
import org.assertj.core.api.Assertions;
import org.glassfish.jersey.server.ResourceConfig;

/** Utility class to test registrations to a {@link ResourceConfig} and a {@link ShutdownHook}. */
public final class RestConfigurationTester {

  private final ResourceConfig resourceConfig;
  private final ShutdownHook shutdownHook;

  private RestConfigurationTester(ResourceConfig resourceConfig, ShutdownHook shutdownHook) {
    this.resourceConfig = resourceConfig;
    this.shutdownHook = shutdownHook;
  }

  /**
   * Create tester.
   *
   * @param resourceConfig the resource config to test
   * @param shutdownHook the shutdown hook to test
   * @return created tester
   */
  public static RestConfigurationTester create(
      ShutdownHook shutdownHook, ResourceConfig resourceConfig) {
    for (AutoCloseable closeable : shutdownHook.closeables()) {
      try {
        closeable.close();
      } catch (Exception e) {
        throw new RuntimeException(e);
      }
    }
    return new RestConfigurationTester(resourceConfig, shutdownHook);
  }

  /**
   * Test resource classes registered on {@link ResourceConfig}.
   *
   * @param expected classes to expect unordered
   * @return this tester
   */
  @CanIgnoreReturnValue
  public RestConfigurationTester expectClasses(Class<?>... expected) {
    Assertions.assertThat(resourceConfig.getClasses())
        .hasSameSizeAs(expected)
        .containsExactlyInAnyOrder(expected);
    return this;
  }

  /**
   * Test classes registered on {@link ShutdownHook}.
   *
   * @param expected classes to expect unordered
   * @return this tester
   */
  @CanIgnoreReturnValue
  public RestConfigurationTester expectAutoCloseables(Class<?>... expected) {
    Assertions.assertThat(shutdownHook.closeables()).hasSameSizeAs(expected);
    for (Class<?> clazz : expected) {
      Assertions.assertThat(shutdownHook.closeables()).anyMatch(clazz::isInstance);
    }
    return this;
  }

  /**
   * Test binder classes registered on {@link ResourceConfig}.
   *
   * @param expected classes to expect unordered
   * @return this tester
   */
  @CanIgnoreReturnValue
  public RestConfigurationTester expectBinders(Class<?>... expected) {
    Assertions.assertThat(resourceConfig.getInstances()).hasSameSizeAs(expected);
    for (Class<?> clazz : expected) {
      Assertions.assertThat(resourceConfig.getInstances()).anyMatch(clazz::isInstance);
    }
    return this;
  }

  /**
   * Test classes bound via a {@link ClassBinder} registered on {@link ResourceConfig}. This will
   * fail if any class is bound to null in the {@link ClassBinder}.
   *
   * @param expected classes to expect unordered
   * @return this tester
   */
  @CanIgnoreReturnValue
  public RestConfigurationTester expectClassBindings(Class<?>... expected) {
    List<RegisteredBinding<?>> bindings = getRegisteredBindings();
    Assertions.assertThat(bindings).hasSameSizeAs(expected);
    for (Class<?> clazz : expected) {
      Assertions.assertThat(bindings).anyMatch(binding -> clazz.equals(binding.clazz));
    }
    return this;
  }

  private List<RegisteredBinding<?>> getRegisteredBindings() {
    for (Object instance : resourceConfig.getInstances()) {
      if (instance instanceof ClassBinder) {
        return ((ClassBinder) instance).getRegisteredBindings();
      }
    }
    return List.of();
  }
}
