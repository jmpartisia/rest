package com.secata.tools.rest.testing.client;

import jakarta.ws.rs.client.Entity;
import jakarta.ws.rs.client.Invocation;
import jakarta.ws.rs.core.GenericType;
import jakarta.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

final class DummyInvocationBuilder extends DefaultInvocationBuilder {

  private static final String GET = "GET";
  private static final String PUT = "PUT";
  private static final String POST = "POST";

  private final List<SentRequest> sentRequests;

  List<SentRequest> getSentRequests() {
    return sentRequests;
  }

  private final Responder<Object> responder;
  private final Map<String, Object> headers;

  private String target;
  private String mediaType;

  @SuppressWarnings("unchecked")
  <T> DummyInvocationBuilder(Responder<T> responder) {
    this.responder = (Responder<Object>) responder;
    this.headers = new HashMap<>();
    this.sentRequests = new ArrayList<>();
  }

  void setTarget(String target) {
    this.target = target;
  }

  void setMediaType(String mediaType) {
    this.mediaType = mediaType;
  }

  @Override
  public Invocation.Builder header(String name, Object value) {
    headers.put(name, value);
    return this;
  }

  @Override
  public Response get() {
    SentRequest sentRequest = new SentRequest(GET, headers, null, target, mediaType);
    this.sentRequests.add(sentRequest);
    return (Response) responder.respond(sentRequest);
  }

  @SuppressWarnings("unchecked")
  @Override
  public <T> T get(Class<T> responseType) {
    SentRequest sentRequest = new SentRequest(GET, headers, null, target, mediaType);
    this.sentRequests.add(sentRequest);
    return (T) responder.respond(sentRequest);
  }

  @SuppressWarnings("unchecked")
  @Override
  public <T> T get(GenericType<T> responseType) {
    SentRequest sentRequest = new SentRequest(GET, headers, null, target, mediaType);
    this.sentRequests.add(sentRequest);
    return (T) responder.respond(sentRequest);
  }

  @Override
  public Response put(Entity<?> entity) {
    SentRequest sentRequest = new SentRequest(PUT, headers, entity, target, mediaType);
    this.sentRequests.add(sentRequest);
    return (Response) responder.respond(sentRequest);
  }

  @SuppressWarnings("unchecked")
  @Override
  public <T> T put(Entity<?> entity, Class<T> responseType) {
    SentRequest sentRequest = new SentRequest(PUT, headers, entity, target, mediaType);
    this.sentRequests.add(sentRequest);
    return (T) responder.respond(sentRequest);
  }

  @SuppressWarnings("unchecked")
  @Override
  public <T> T put(Entity<?> entity, GenericType<T> responseType) {
    SentRequest sentRequest = new SentRequest(PUT, headers, entity, target, mediaType);
    this.sentRequests.add(sentRequest);
    return (T) responder.respond(sentRequest);
  }

  @Override
  public Response post(Entity<?> entity) {
    SentRequest sentRequest = new SentRequest(POST, headers, entity, target, mediaType);
    this.sentRequests.add(sentRequest);
    return (Response) responder.respond(sentRequest);
  }

  @SuppressWarnings("unchecked")
  @Override
  public <T> T post(Entity<?> entity, Class<T> responseType) {
    SentRequest sentRequest = new SentRequest(POST, headers, entity, target, mediaType);
    this.sentRequests.add(sentRequest);
    return (T) responder.respond(sentRequest);
  }

  @SuppressWarnings("unchecked")
  @Override
  public <T> T post(Entity<?> entity, GenericType<T> responseType) {
    SentRequest sentRequest = new SentRequest(POST, headers, entity, target, mediaType);
    this.sentRequests.add(sentRequest);
    return (T) responder.respond(sentRequest);
  }
}
