package com.secata.tools.rest.testing.client;

import jakarta.ws.rs.client.Invocation;
import jakarta.ws.rs.client.WebTarget;
import jakarta.ws.rs.core.MediaType;
import java.util.Arrays;

final class DummyWebTarget extends DefaultWebTarget implements WebTarget {

  private final DummyInvocationBuilder invocationBuilder;

  DummyWebTarget(DummyInvocationBuilder invocationBuilder) {
    this.invocationBuilder = invocationBuilder;
  }

  @Override
  public Invocation.Builder request() {
    return invocationBuilder;
  }

  @Override
  public Invocation.Builder request(String... acceptedResponseTypes) {
    if (acceptedResponseTypes.length > 1) {
      throw new UnsupportedOperationException(
          "Currently, only a single accepted response type is supported");
    }
    invocationBuilder.setMediaType(acceptedResponseTypes[0]);
    return invocationBuilder;
  }

  @Override
  public Invocation.Builder request(MediaType... acceptedResponseTypes) {
    String[] stringResponseTypes =
        Arrays.stream(acceptedResponseTypes).map(MediaType::toString).toArray(String[]::new);
    return request(stringResponseTypes);
  }
}
