package com.secata.tools.rest.testing;

import jakarta.ws.rs.RedirectionException;
import jakarta.ws.rs.WebApplicationException;
import jakarta.ws.rs.core.Response;
import jakarta.ws.rs.core.Response.Status;
import java.util.Objects;
import org.assertj.core.api.Condition;

/** Shorthands for testing rest response output. */
public final class RestStatus {

  private RestStatus() {}

  static boolean compareResponseWithStatus(Response response, Status status) {
    if (status == null) {
      return false;
    } else if (response == null) {
      return false;
    } else {
      return Objects.equals(
          Objects.requireNonNull(status), Objects.requireNonNull(response).getStatusInfo());
    }
  }

  static Condition<Response> result(Status status) {
    return new Condition<>() {
      @Override
      public boolean matches(Response value) {
        return compareResponseWithStatus(value, status);
      }
    };
  }

  /** Assertj condition for check result is OK. */
  public static final Condition<Response> OK = result(Status.OK);

  /** Assertj condition for check result is CREATED. */
  public static final Condition<Response> CREATED = result(Status.CREATED);

  /** Assertj condition for check result is ACCEPTED. */
  public static final Condition<Response> ACCEPTED = result(Status.ACCEPTED);

  /** Assertj condition for check result is NO_CONTENT. */
  public static final Condition<Response> NO_CONTENT = result(Status.NO_CONTENT);

  /** Assertj condition for check result is RESET_CONTENT. */
  public static final Condition<Response> RESET_CONTENT = result(Status.RESET_CONTENT);

  /** Assertj condition for check result is PARTIAL_CONTENT. */
  public static final Condition<Response> PARTIAL_CONTENT = result(Status.PARTIAL_CONTENT);

  static <T> Condition<T> redirect(Status status) {
    return new Condition<>() {
      @Override
      public boolean matches(T value) {
        if (value == null) {
          return false;
        } else if (value instanceof Response) {
          return compareResponseWithStatus((Response) value, status);
        } else if (value instanceof RedirectionException) {
          Response response = ((RedirectionException) value).getResponse();
          return compareResponseWithStatus(response, status);
        } else {
          return false;
        }
      }
    };
  }

  /** Assertj condition for check object is MOVED_PERMANENTLY. */
  public static final Condition<Object> MOVED_PERMANENTLY = redirect(Status.MOVED_PERMANENTLY);

  /** Assertj condition for check object is FOUND. */
  public static final Condition<Object> FOUND = redirect(Status.FOUND);

  /** Assertj condition for check object is SEE_OTHER. */
  public static final Condition<Object> SEE_OTHER = redirect(Status.SEE_OTHER);

  /** Assertj condition for check object is NOT_MODIFIED. */
  public static final Condition<Object> NOT_MODIFIED = redirect(Status.NOT_MODIFIED);

  /** Assertj condition for check object is USE_PROXY. */
  public static final Condition<Object> USE_PROXY = redirect(Status.USE_PROXY);

  /** Assertj condition for check object is TEMPORARY_REDIRECT. */
  public static final Condition<Object> TEMPORARY_REDIRECT = redirect(Status.TEMPORARY_REDIRECT);

  static Condition<Throwable> error(Status status) {
    return new Condition<>() {
      @Override
      public boolean matches(Throwable value) {
        if (value == null) {
          return false;
        } else if (value instanceof WebApplicationException) {
          Response response = ((WebApplicationException) value).getResponse();
          return compareResponseWithStatus(response, status);
        } else {
          return false;
        }
      }
    };
  }

  /** Assertj condition for call result is an exception typed with BAD_REQUEST. */
  public static final Condition<Throwable> BAD_REQUEST = error(Status.BAD_REQUEST);

  /** Assertj condition for call result is an exception typed with UNAUTHORIZED. */
  public static final Condition<Throwable> UNAUTHORIZED = error(Status.UNAUTHORIZED);

  /** Assertj condition for call result is an exception typed with PAYMENT_REQUIRED. */
  public static final Condition<Throwable> PAYMENT_REQUIRED = error(Status.PAYMENT_REQUIRED);

  /** Assertj condition for call result is an exception typed with FORBIDDEN. */
  public static final Condition<Throwable> FORBIDDEN = error(Status.FORBIDDEN);

  /** Assertj condition for call result is an exception typed with NOT_FOUND. */
  public static final Condition<Throwable> NOT_FOUND = error(Status.NOT_FOUND);

  /** Assertj condition for call result is an exception typed with METHOD_NOT_ALLOWED. */
  public static final Condition<Throwable> METHOD_NOT_ALLOWED = error(Status.METHOD_NOT_ALLOWED);

  /** Assertj condition for call result is an exception typed with NOT_ACCEPTABLE. */
  public static final Condition<Throwable> NOT_ACCEPTABLE = error(Status.NOT_ACCEPTABLE);

  /** Assertj condition for call result is an exception typed with PROXY_AUTHENTICATION_REQUIRED. */
  public static final Condition<Throwable> PROXY_AUTHENTICATION_REQUIRED =
      error(Status.PROXY_AUTHENTICATION_REQUIRED);

  /** Assertj condition for call result is an exception typed with REQUEST_TIMEOUT. */
  public static final Condition<Throwable> REQUEST_TIMEOUT = error(Status.REQUEST_TIMEOUT);

  /** Assertj condition for call result is an exception typed with CONFLICT. */
  public static final Condition<Throwable> CONFLICT = error(Status.CONFLICT);

  /** Assertj condition for call result is an exception typed with GONE. */
  public static final Condition<Throwable> GONE = error(Status.GONE);

  /** Assertj condition for call result is an exception typed with LENGTH_REQUIRED. */
  public static final Condition<Throwable> LENGTH_REQUIRED = error(Status.LENGTH_REQUIRED);

  /** Assertj condition for call result is an exception typed with PRECONDITION_FAILED. */
  public static final Condition<Throwable> PRECONDITION_FAILED = error(Status.PRECONDITION_FAILED);

  /** Assertj condition for call result is an exception typed with REQUEST_ENTITY_TOO_LARGE. */
  public static final Condition<Throwable> REQUEST_ENTITY_TOO_LARGE =
      error(Status.REQUEST_ENTITY_TOO_LARGE);

  /** Assertj condition for call result is an exception typed with REQUEST_URI_TOO_LONG. */
  public static final Condition<Throwable> REQUEST_URI_TOO_LONG =
      error(Status.REQUEST_URI_TOO_LONG);

  /** Assertj condition for call result is an exception typed with UNSUPPORTED_MEDIA_TYPE. */
  public static final Condition<Throwable> UNSUPPORTED_MEDIA_TYPE =
      error(Status.UNSUPPORTED_MEDIA_TYPE);

  /**
   * Assertj condition for call result is an exception typed with REQUESTED_RANGE_NOT_SATISFIABLE.
   */
  public static final Condition<Throwable> REQUESTED_RANGE_NOT_SATISFIABLE =
      error(Status.REQUESTED_RANGE_NOT_SATISFIABLE);

  /** Assertj condition for call result is an exception typed with EXPECTATION_FAILED. */
  public static final Condition<Throwable> EXPECTATION_FAILED = error(Status.EXPECTATION_FAILED);

  /** Assertj condition for call result is an exception typed with PRECONDITION_REQUIRED. */
  public static final Condition<Throwable> PRECONDITION_REQUIRED =
      error(Status.PRECONDITION_REQUIRED);

  /** Assertj condition for call result is an exception typed with TOO_MANY_REQUESTS. */
  public static final Condition<Throwable> TOO_MANY_REQUESTS = error(Status.TOO_MANY_REQUESTS);

  /**
   * Assertj condition for call result is an exception typed with REQUEST_HEADER_FIELDS_TOO_LARGE.
   */
  public static final Condition<Throwable> REQUEST_HEADER_FIELDS_TOO_LARGE =
      error(Status.REQUEST_HEADER_FIELDS_TOO_LARGE);

  /** Assertj condition for call result is an exception typed with INTERNAL_SERVER_ERROR. */
  public static final Condition<Throwable> INTERNAL_SERVER_ERROR =
      error(Status.INTERNAL_SERVER_ERROR);

  /** Assertj condition for call result is an exception typed with NOT_IMPLEMENTED. */
  public static final Condition<Throwable> NOT_IMPLEMENTED = error(Status.NOT_IMPLEMENTED);

  /** Assertj condition for call result is an exception typed with BAD_GATEWAY. */
  public static final Condition<Throwable> BAD_GATEWAY = error(Status.BAD_GATEWAY);

  /** Assertj condition for call result is an exception typed with SERVICE_UNAVAILABLE. */
  public static final Condition<Throwable> SERVICE_UNAVAILABLE = error(Status.SERVICE_UNAVAILABLE);

  /** Assertj condition for call result is an exception typed with GATEWAY_TIMEOUT. */
  public static final Condition<Throwable> GATEWAY_TIMEOUT = error(Status.GATEWAY_TIMEOUT);

  /** Assertj condition for call result is an exception typed with HTTP_VERSION_NOT_SUPPORTED. */
  public static final Condition<Throwable> HTTP_VERSION_NOT_SUPPORTED =
      error(Status.HTTP_VERSION_NOT_SUPPORTED);

  /**
   * Assertj condition for call result is an exception typed with NETWORK_AUTHENTICATION_REQUIRED.
   */
  public static final Condition<Throwable> NETWORK_AUTHENTICATION_REQUIRED =
      error(Status.NETWORK_AUTHENTICATION_REQUIRED);
}
