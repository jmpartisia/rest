package com.secata.tools.rest.testing.client;

import jakarta.ws.rs.client.AsyncInvoker;
import jakarta.ws.rs.client.CompletionStageRxInvoker;
import jakarta.ws.rs.client.Entity;
import jakarta.ws.rs.client.Invocation;
import jakarta.ws.rs.client.RxInvoker;
import jakarta.ws.rs.core.CacheControl;
import jakarta.ws.rs.core.Cookie;
import jakarta.ws.rs.core.GenericType;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.MultivaluedMap;
import jakarta.ws.rs.core.Response;
import java.util.Locale;

/** Abstract class with default implementations of all methods of {@link Invocation.Builder}. */
abstract class DefaultInvocationBuilder implements Invocation.Builder {

  @Override
  public Invocation.Builder header(String name, Object value) {
    throw new UnsupportedOperationException();
  }

  @Override
  public Invocation.Builder headers(MultivaluedMap<String, Object> headers) {
    throw new UnsupportedOperationException();
  }

  @Override
  public final Invocation build(String method) {
    throw new UnsupportedOperationException();
  }

  @Override
  public final Invocation build(String method, Entity<?> entity) {
    throw new UnsupportedOperationException();
  }

  @Override
  public final Invocation buildGet() {
    throw new UnsupportedOperationException();
  }

  @Override
  public final Invocation buildDelete() {
    throw new UnsupportedOperationException();
  }

  @Override
  public final Invocation buildPost(Entity<?> entity) {
    throw new UnsupportedOperationException();
  }

  @Override
  public final Invocation buildPut(Entity<?> entity) {
    throw new UnsupportedOperationException();
  }

  @Override
  public final AsyncInvoker async() {
    throw new UnsupportedOperationException();
  }

  @Override
  public final Invocation.Builder accept(String... mediaTypes) {
    throw new UnsupportedOperationException();
  }

  @Override
  public final Invocation.Builder accept(MediaType... mediaTypes) {
    throw new UnsupportedOperationException();
  }

  @Override
  public final Invocation.Builder acceptLanguage(Locale... locales) {
    throw new UnsupportedOperationException();
  }

  @Override
  public final Invocation.Builder acceptLanguage(String... locales) {
    throw new UnsupportedOperationException();
  }

  @Override
  public final Invocation.Builder acceptEncoding(String... encodings) {
    throw new UnsupportedOperationException();
  }

  @Override
  public final Invocation.Builder cookie(Cookie cookie) {
    throw new UnsupportedOperationException();
  }

  @Override
  public final Invocation.Builder cookie(String name, String value) {
    throw new UnsupportedOperationException();
  }

  @Override
  public final Invocation.Builder cacheControl(CacheControl cacheControl) {
    throw new UnsupportedOperationException();
  }

  @Override
  public final Invocation.Builder property(String name, Object value) {
    throw new UnsupportedOperationException();
  }

  @Override
  public final CompletionStageRxInvoker rx() {
    throw new UnsupportedOperationException();
  }

  @SuppressWarnings("rawtypes")
  @Override
  public final <T extends RxInvoker> T rx(Class<T> clazz) {
    throw new UnsupportedOperationException();
  }

  @Override
  public final Response delete() {
    throw new UnsupportedOperationException();
  }

  @Override
  public final <T> T delete(Class<T> responseType) {
    throw new UnsupportedOperationException();
  }

  @Override
  public final <T> T delete(GenericType<T> responseType) {
    throw new UnsupportedOperationException();
  }

  @Override
  public final Response head() {
    throw new UnsupportedOperationException();
  }

  @Override
  public final Response options() {
    throw new UnsupportedOperationException();
  }

  @Override
  public final <T> T options(Class<T> responseType) {
    throw new UnsupportedOperationException();
  }

  @Override
  public final <T> T options(GenericType<T> responseType) {
    throw new UnsupportedOperationException();
  }

  @Override
  public final Response trace() {
    throw new UnsupportedOperationException();
  }

  @Override
  public final <T> T trace(Class<T> responseType) {
    throw new UnsupportedOperationException();
  }

  @Override
  public final <T> T trace(GenericType<T> responseType) {
    throw new UnsupportedOperationException();
  }

  @Override
  public final Response method(String name) {
    throw new UnsupportedOperationException();
  }

  @Override
  public final <T> T method(String name, Class<T> responseType) {
    throw new UnsupportedOperationException();
  }

  @Override
  public final <T> T method(String name, GenericType<T> responseType) {
    throw new UnsupportedOperationException();
  }

  @Override
  public final Response method(String name, Entity<?> entity) {
    throw new UnsupportedOperationException();
  }

  @Override
  public final <T> T method(String name, Entity<?> entity, Class<T> responseType) {
    throw new UnsupportedOperationException();
  }

  @Override
  public final <T> T method(String name, Entity<?> entity, GenericType<T> responseType) {
    throw new UnsupportedOperationException();
  }

  @Override
  public Response get() {
    throw new UnsupportedOperationException();
  }

  @Override
  public <T> T get(Class<T> responseType) {
    throw new UnsupportedOperationException();
  }

  @Override
  public <T> T get(GenericType<T> responseType) {
    throw new UnsupportedOperationException();
  }

  @Override
  public Response put(Entity<?> entity) {
    throw new UnsupportedOperationException();
  }

  @Override
  public <T> T put(Entity<?> entity, Class<T> responseType) {
    throw new UnsupportedOperationException();
  }

  @Override
  public <T> T put(Entity<?> entity, GenericType<T> responseType) {
    throw new UnsupportedOperationException();
  }

  @Override
  public Response post(Entity<?> entity) {
    throw new UnsupportedOperationException();
  }

  @Override
  public <T> T post(Entity<?> entity, Class<T> responseType) {
    throw new UnsupportedOperationException();
  }

  @Override
  public <T> T post(Entity<?> entity, GenericType<T> responseType) {
    throw new UnsupportedOperationException();
  }
}
