package com.secata.tools.rest.testing.client;

import jakarta.ws.rs.client.Client;
import jakarta.ws.rs.client.Invocation;
import jakarta.ws.rs.client.WebTarget;
import jakarta.ws.rs.core.Link;
import jakarta.ws.rs.core.UriBuilder;
import java.net.URI;
import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLContext;

/** Abstract class with default implementations of all methods of {@link Client }. */
abstract class DefaultClient extends DefaultConfigurable<Client> implements Client {

  @Override
  public void close() {
    throw new UnsupportedOperationException();
  }

  @Override
  public WebTarget target(String uri) {
    throw new UnsupportedOperationException();
  }

  @Override
  public WebTarget target(URI uri) {
    throw new UnsupportedOperationException();
  }

  @Override
  public WebTarget target(UriBuilder uriBuilder) {
    throw new UnsupportedOperationException();
  }

  @Override
  public WebTarget target(Link link) {
    throw new UnsupportedOperationException();
  }

  @Override
  public Invocation.Builder invocation(Link link) {
    throw new UnsupportedOperationException();
  }

  @Override
  public SSLContext getSslContext() {
    throw new UnsupportedOperationException();
  }

  @Override
  public HostnameVerifier getHostnameVerifier() {
    throw new UnsupportedOperationException();
  }
}
