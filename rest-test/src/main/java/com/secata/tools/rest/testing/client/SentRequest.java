package com.secata.tools.rest.testing.client;

import jakarta.ws.rs.client.Entity;
import java.util.Map;

/** Record containing information about a sent request. */
public record SentRequest(
    String method,
    Map<String, Object> headers,
    Entity<?> entity,
    String target,
    String mediaType) {}
