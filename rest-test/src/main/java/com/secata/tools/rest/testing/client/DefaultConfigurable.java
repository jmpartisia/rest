package com.secata.tools.rest.testing.client;

import jakarta.ws.rs.core.Configurable;
import jakarta.ws.rs.core.Configuration;
import java.util.Map;

/** Abstract class with default implementations of all methods of {@link Configurable}. */
abstract class DefaultConfigurable<C extends Configurable<C>> implements Configurable<C> {

  @Override
  public Configuration getConfiguration() {
    throw new UnsupportedOperationException();
  }

  @Override
  public C property(String name, Object value) {
    throw new UnsupportedOperationException();
  }

  @Override
  public C register(Class<?> componentClass) {
    throw new UnsupportedOperationException();
  }

  @Override
  public C register(Class<?> componentClass, int priority) {
    throw new UnsupportedOperationException();
  }

  @Override
  public C register(Class<?> componentClass, Class<?>... contracts) {
    throw new UnsupportedOperationException();
  }

  @Override
  public C register(Class<?> componentClass, Map<Class<?>, Integer> contracts) {
    throw new UnsupportedOperationException();
  }

  @Override
  public C register(Object component) {
    throw new UnsupportedOperationException();
  }

  @Override
  public C register(Object component, int priority) {
    throw new UnsupportedOperationException();
  }

  @Override
  public C register(Object component, Class<?>... contracts) {
    throw new UnsupportedOperationException();
  }

  @Override
  public C register(Object component, Map<Class<?>, Integer> contracts) {
    throw new UnsupportedOperationException();
  }
}
