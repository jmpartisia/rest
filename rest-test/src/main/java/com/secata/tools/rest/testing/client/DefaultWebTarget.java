package com.secata.tools.rest.testing.client;

import jakarta.ws.rs.client.Invocation;
import jakarta.ws.rs.client.WebTarget;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.UriBuilder;
import java.net.URI;
import java.util.Map;

/** Abstract class with default implementations of all methods of {@link WebTarget}. */
abstract class DefaultWebTarget extends DefaultConfigurable<WebTarget> implements WebTarget {

  @Override
  public URI getUri() {
    throw new UnsupportedOperationException();
  }

  @Override
  public UriBuilder getUriBuilder() {
    throw new UnsupportedOperationException();
  }

  @Override
  public WebTarget path(String path) {
    throw new UnsupportedOperationException();
  }

  @Override
  public WebTarget resolveTemplate(String name, Object value) {
    throw new UnsupportedOperationException();
  }

  @Override
  public WebTarget resolveTemplate(String name, Object value, boolean encodeSlashInPath) {
    throw new UnsupportedOperationException();
  }

  @Override
  public WebTarget resolveTemplateFromEncoded(String name, Object value) {
    throw new UnsupportedOperationException();
  }

  @Override
  public WebTarget resolveTemplates(Map<String, Object> templateValues) {
    throw new UnsupportedOperationException();
  }

  @Override
  public WebTarget resolveTemplates(Map<String, Object> templateValues, boolean encodeSlashInPath) {
    throw new UnsupportedOperationException();
  }

  @Override
  public WebTarget resolveTemplatesFromEncoded(Map<String, Object> templateValues) {
    throw new UnsupportedOperationException();
  }

  @Override
  public WebTarget matrixParam(String name, Object... values) {
    throw new UnsupportedOperationException();
  }

  @Override
  public WebTarget queryParam(String name, Object... values) {
    throw new UnsupportedOperationException();
  }

  @Override
  public Invocation.Builder request() {
    throw new UnsupportedOperationException();
  }

  @Override
  public Invocation.Builder request(String... acceptedResponseTypes) {
    throw new UnsupportedOperationException();
  }

  @Override
  public Invocation.Builder request(MediaType... acceptedResponseTypes) {
    throw new UnsupportedOperationException();
  }
}
