package com.secata.tools.rest.testing.client;

/**
 * Functional interface returning a response given a sent request.
 *
 * @param <T> the type of the response
 */
@FunctionalInterface
public interface Responder<T> {

  /**
   * Returns a response given a sent request.
   *
   * @param sentRequest the sent request
   * @return the response to the sent request
   */
  T respond(SentRequest sentRequest);
}
