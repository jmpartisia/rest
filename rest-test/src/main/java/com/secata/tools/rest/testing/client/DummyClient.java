package com.secata.tools.rest.testing.client;

import jakarta.ws.rs.client.Client;
import jakarta.ws.rs.client.WebTarget;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/** Client implementation to be used in tests. */
public final class DummyClient extends DefaultClient implements Client {

  private final Map<String, Tuple> tuples;

  private DummyClient(Map<String, Tuple> tuples) {
    this.tuples = tuples;
  }

  @Override
  public WebTarget target(String uri) {
    Tuple tuple = tuples.get(uri);
    tuple.invocationBuilder.setTarget(uri);
    return tuple.webTarget();
  }

  /**
   * Gets the record of meta-data of the request sent to the given url.
   *
   * <p>If more than a single request use {@link #getSentRequests} instead.
   *
   * @param url the target url of the sent request
   * @return the sent request
   */
  public SentRequest getSentRequest(String url) {
    List<SentRequest> sentRequests = getSentRequests(url);
    if (sentRequests.size() > 1) {
      throw new IllegalStateException("More than one request was sent to the specified URL");
    } else {
      return sentRequests.get(0);
    }
  }

  /**
   * Gets the list of all records of meta-data of the requests sent to the given url.
   *
   * @param url the target url of the sent requests
   * @return the sent requests
   */
  public List<SentRequest> getSentRequests(String url) {
    return tuples.get(url).invocationBuilder().getSentRequests();
  }

  /**
   * Create a DummyClient builder.
   *
   * @return the created builder
   */
  public static Builder create() {
    return new Builder();
  }

  /** Test entry point to the client API used to bootstrap a DummyClient instance. */
  public static final class Builder {

    private final Map<String, Tuple> tuples;

    private Builder() {
      this.tuples = new HashMap<>();
    }

    /**
     * Prepare response for the dummy client. Requests to the given url all respond with the given
     * response.
     *
     * @param url the url
     * @param response the response
     * @param <T> the type of the response
     * @return this builder
     */
    public <T> Builder prepareResponse(String url, T response) {
      return prepareResponder(url, (request) -> response);
    }

    /**
     * Prepare responder for the dummy client. The responder is called with requests sent to the
     * given url to get the response.
     *
     * @param url the url
     * @param responder the derives the response given the sent request
     * @param <T> the type of the response
     * @return this builder
     */
    public <T> Builder prepareResponder(String url, Responder<T> responder) {
      DummyInvocationBuilder invocationBuilder = new DummyInvocationBuilder(responder);
      Tuple tuple = new Tuple(new DummyWebTarget(invocationBuilder), invocationBuilder);
      if (tuples.containsKey(url)) {
        throw new IllegalArgumentException("Cannot prepare for the same URL twice");
      }
      tuples.put(url, tuple);
      return this;
    }

    /**
     * Build a new dummy client instance using all the configuration previously specified in this
     * client builder.
     *
     * @return a new dummy client instance
     */
    public DummyClient build() {
      return new DummyClient(Map.copyOf(tuples));
    }
  }

  record Tuple(DummyWebTarget webTarget, DummyInvocationBuilder invocationBuilder) {}
}
