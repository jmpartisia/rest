package com.secata.tools.rest.testing.client;

import static org.assertj.core.api.Assertions.assertThatThrownBy;

import jakarta.ws.rs.client.WebTarget;
import java.util.Map;
import org.assertj.core.api.ThrowableAssert;
import org.junit.jupiter.api.Test;

/** Test. */
final class DefaultConfigurableTest {

  @Test
  void unsupportedOperations() {
    DefaultConfigurable<WebTarget> impl = new DefaultConfigurable<>() {};

    final Object component = "component";

    assertUnsupported(impl::getConfiguration);
    assertUnsupported(() -> impl.property("", ""));
    assertUnsupported(() -> impl.register(String.class));
    assertUnsupported(() -> impl.register(String.class, 1));
    assertUnsupported(() -> impl.register(String.class, Integer.class));
    assertUnsupported(() -> impl.register(String.class, Map.of(Integer.class, 1)));
    assertUnsupported(() -> impl.register(component));
    assertUnsupported(() -> impl.register(component, 1));
    assertUnsupported(() -> impl.register(component, Integer.class));
    assertUnsupported(() -> impl.register(component, Map.of(Integer.class, 1)));
  }

  private void assertUnsupported(ThrowableAssert.ThrowingCallable shouldRaiseThrowable) {
    assertThatThrownBy(shouldRaiseThrowable).isInstanceOf(UnsupportedOperationException.class);
  }
}
