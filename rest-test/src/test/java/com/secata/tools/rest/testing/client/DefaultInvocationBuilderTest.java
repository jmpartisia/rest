package com.secata.tools.rest.testing.client;

import static org.assertj.core.api.Assertions.assertThatThrownBy;

import jakarta.ws.rs.client.Entity;
import jakarta.ws.rs.client.RxInvoker;
import jakarta.ws.rs.core.CacheControl;
import jakarta.ws.rs.core.Cookie;
import jakarta.ws.rs.core.GenericType;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.MultivaluedHashMap;
import jakarta.ws.rs.core.Response;
import java.util.Locale;
import org.assertj.core.api.ThrowableAssert;
import org.junit.jupiter.api.Test;

/** Test. */
final class DefaultInvocationBuilderTest {

  @SuppressWarnings({"Convert2MethodRef", "resource"})
  @Test
  void unsupportedOperations() {
    DefaultInvocationBuilder impl = new DefaultInvocationBuilder() {};

    final GenericType<Response> responseType = new GenericType<>() {};
    final Entity<String> entity = Entity.text("entity");

    assertUnsupported(() -> impl.header("name", "value"));
    assertUnsupported(() -> impl.headers(new MultivaluedHashMap<>()));
    assertUnsupported(() -> impl.build("GET"));
    assertUnsupported(() -> impl.build("GET", entity));
    assertUnsupported(() -> impl.buildGet());
    assertUnsupported(() -> impl.buildDelete());
    assertUnsupported(() -> impl.buildPost(entity));
    assertUnsupported(() -> impl.buildPut(entity));
    assertUnsupported(() -> impl.async());
    assertUnsupported(() -> impl.accept(MediaType.WILDCARD));
    assertUnsupported(() -> impl.accept(MediaType.WILDCARD_TYPE));
    assertUnsupported(() -> impl.acceptLanguage(Locale.US));
    assertUnsupported(() -> impl.acceptLanguage("en"));
    assertUnsupported(() -> impl.acceptEncoding("gzip"));
    assertUnsupported(() -> impl.cookie(new Cookie.Builder("name").build()));
    assertUnsupported(() -> impl.cookie("name", "value"));
    assertUnsupported(() -> impl.cacheControl(new CacheControl()));
    assertUnsupported(() -> impl.property("name", "value"));
    assertUnsupported(() -> impl.rx());
    assertUnsupported(() -> impl.rx(RxInvoker.class));
    assertUnsupported(() -> impl.delete());
    assertUnsupported(() -> impl.delete(Response.class));
    assertUnsupported(() -> impl.delete(responseType));
    assertUnsupported(() -> impl.head());
    assertUnsupported(() -> impl.options());
    assertUnsupported(() -> impl.options(Response.class));
    assertUnsupported(() -> impl.options(responseType));
    assertUnsupported(() -> impl.trace());
    assertUnsupported(() -> impl.trace(Response.class));
    assertUnsupported(() -> impl.trace(responseType));
    assertUnsupported(() -> impl.method("GET"));
    assertUnsupported(() -> impl.method("GET", Response.class));
    assertUnsupported(() -> impl.method("GET", responseType));
    assertUnsupported(() -> impl.method("POST", entity));
    assertUnsupported(() -> impl.method("POST", entity, Response.class));
    assertUnsupported(() -> impl.method("POST", entity, responseType));
    assertUnsupported(() -> impl.get());
    assertUnsupported(() -> impl.get(Response.class));
    assertUnsupported(() -> impl.get(responseType));
    assertUnsupported(() -> impl.put(entity));
    assertUnsupported(() -> impl.put(entity, Response.class));
    assertUnsupported(() -> impl.put(entity, responseType));
    assertUnsupported(() -> impl.post(entity));
    assertUnsupported(() -> impl.post(entity, Response.class));
    assertUnsupported(() -> impl.post(entity, responseType));
  }

  private void assertUnsupported(ThrowableAssert.ThrowingCallable shouldRaiseThrowable) {
    assertThatThrownBy(shouldRaiseThrowable).isInstanceOf(UnsupportedOperationException.class);
  }
}
