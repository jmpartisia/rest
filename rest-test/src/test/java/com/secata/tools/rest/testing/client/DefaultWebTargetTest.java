package com.secata.tools.rest.testing.client;

import static org.assertj.core.api.Assertions.assertThatThrownBy;

import jakarta.ws.rs.core.MediaType;
import java.util.Map;
import org.assertj.core.api.ThrowableAssert;
import org.junit.jupiter.api.Test;

/** Test. */
final class DefaultWebTargetTest {

  @SuppressWarnings("Convert2MethodRef")
  @Test
  void unsupportedOperation() {
    DefaultWebTarget impl = new DefaultWebTarget() {};

    assertUnsupported(() -> impl.getUri());
    assertUnsupported(() -> impl.getUriBuilder());
    assertUnsupported(() -> impl.path("path"));
    assertUnsupported(() -> impl.resolveTemplate("name", "value"));
    assertUnsupported(() -> impl.resolveTemplate("name", "value", true));
    assertUnsupported(() -> impl.resolveTemplateFromEncoded("name", "value"));
    assertUnsupported(() -> impl.resolveTemplates(Map.of()));
    assertUnsupported(() -> impl.resolveTemplates(Map.of(), true));
    assertUnsupported(() -> impl.resolveTemplatesFromEncoded(Map.of()));
    assertUnsupported(() -> impl.matrixParam("name", 1, 2));
    assertUnsupported(() -> impl.queryParam("name", 1, 2));
    assertUnsupported(() -> impl.request());
    assertUnsupported(() -> impl.request(MediaType.WILDCARD));
    assertUnsupported(() -> impl.request(MediaType.WILDCARD_TYPE));
  }

  private void assertUnsupported(ThrowableAssert.ThrowingCallable shouldRaiseThrowable) {
    assertThatThrownBy(shouldRaiseThrowable).isInstanceOf(UnsupportedOperationException.class);
  }
}
