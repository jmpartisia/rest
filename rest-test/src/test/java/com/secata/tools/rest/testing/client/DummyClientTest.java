package com.secata.tools.rest.testing.client;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

import jakarta.ws.rs.client.Entity;
import jakarta.ws.rs.core.GenericType;
import jakarta.ws.rs.core.HttpHeaders;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;
import java.util.List;
import java.util.concurrent.atomic.AtomicLong;
import org.junit.jupiter.api.Test;

/** Test. */
final class DummyClientTest {

  @Test
  void get() {
    Response expected = Response.status(Response.Status.OK).build();
    String url = "/get";
    DummyClient client = DummyClient.create().prepareResponse(url, expected).build();

    Response response =
        client.target(url).request().header(HttpHeaders.AUTHORIZATION, "auth").get();

    assertThat(response).isEqualTo(expected);

    SentRequest sentRequest = client.getSentRequest(url);
    assertThat(sentRequest.method()).isEqualTo("GET");
    assertThat(sentRequest.headers()).hasSize(1).containsEntry(HttpHeaders.AUTHORIZATION, "auth");
    assertThat(sentRequest.entity()).isNull();
    assertThat(sentRequest.target()).isEqualTo(url);
    assertThat(sentRequest.mediaType()).isNull();
  }

  @Test
  void getSameUrlTwiceWithDifferentResponses() {
    String url = "/url";

    AtomicLong num = new AtomicLong();

    DummyClient client =
        DummyClient.create().prepareResponder(url, (request) -> num.incrementAndGet()).build();

    Long first = client.target(url).request().get(Long.class);
    Long second = client.target(url).request().get(Long.class);

    assertThat(first).isEqualTo(1L);
    assertThat(second).isEqualTo(2L);

    List<SentRequest> sentRequests = client.getSentRequests(url);
    assertThat(sentRequests)
        .hasSize(2)
        .allSatisfy(
            sentRequest -> {
              assertThat(sentRequest.method()).isEqualTo("GET");
              assertThat(sentRequest.headers()).isEmpty();
              assertThat(sentRequest.entity()).isNull();
              assertThat(sentRequest.target()).isEqualTo(url);
              assertThat(sentRequest.mediaType()).isNull();
            });
  }

  @Test
  void getClassType() {
    long expected = 123L;
    String url = "/get";
    DummyClient client = DummyClient.create().prepareResponse(url, expected).build();

    Long response = client.target(url).request(MediaType.APPLICATION_JSON_TYPE).get(Long.class);

    assertThat(response).isEqualTo(expected);

    SentRequest sentRequest = client.getSentRequest(url);
    assertThat(sentRequest.method()).isEqualTo("GET");
    assertThat(sentRequest.headers()).isEmpty();
    assertThat(sentRequest.entity()).isNull();
    assertThat(sentRequest.target()).isEqualTo(url);
    assertThat(sentRequest.mediaType()).isEqualTo(MediaType.APPLICATION_JSON);
  }

  @Test
  void getGenericType() {
    long expected = 123L;
    String url = "/get";
    DummyClient client = DummyClient.create().prepareResponse(url, expected).build();

    Long response =
        client.target(url).request(MediaType.APPLICATION_JSON).get(new GenericType<>() {});

    assertThat(response).isEqualTo(expected);

    SentRequest sentRequest = client.getSentRequest(url);
    assertThat(sentRequest.method()).isEqualTo("GET");
    assertThat(sentRequest.headers()).isEmpty();
    assertThat(sentRequest.entity()).isNull();
    assertThat(sentRequest.target()).isEqualTo(url);
    assertThat(sentRequest.mediaType()).isEqualTo(MediaType.APPLICATION_JSON);
  }

  @Test
  void put() {
    Response expected = Response.status(Response.Status.NO_CONTENT).build();
    String url = "/put";
    DummyClient client = DummyClient.create().prepareResponse(url, expected).build();

    Entity<String> entity = Entity.text("entity");

    Response response = client.target(url).request().put(entity);

    assertThat(response).isEqualTo(expected);

    SentRequest sentRequest = client.getSentRequest(url);
    assertThat(sentRequest.method()).isEqualTo("PUT");
    assertThat(sentRequest.headers()).isEmpty();
    assertThat(sentRequest.entity()).isEqualTo(entity);
    assertThat(sentRequest.target()).isEqualTo(url);
    assertThat(sentRequest.mediaType()).isNull();
  }

  @Test
  void putClassType() {
    String expected = "expected response";
    String url = "/put";
    DummyClient client = DummyClient.create().prepareResponse(url, expected).build();

    Entity<String> entity = Entity.text("entity");

    String response = client.target(url).request(MediaType.TEXT_PLAIN).put(entity, String.class);

    assertThat(response).isEqualTo(expected);

    SentRequest sentRequest = client.getSentRequest(url);
    assertThat(sentRequest.method()).isEqualTo("PUT");
    assertThat(sentRequest.headers()).isEmpty();
    assertThat(sentRequest.entity()).isEqualTo(entity);
    assertThat(sentRequest.target()).isEqualTo(url);
    assertThat(sentRequest.mediaType()).isEqualTo(MediaType.TEXT_PLAIN);
  }

  @Test
  void putGenericType() {
    String expected = "expected response";
    String url = "/put";
    DummyClient client = DummyClient.create().prepareResponse(url, expected).build();

    Entity<String> entity = Entity.text("entity");

    String response =
        client.target(url).request(MediaType.TEXT_PLAIN).put(entity, new GenericType<>() {});

    assertThat(response).isEqualTo(expected);

    SentRequest sentRequest = client.getSentRequest(url);
    assertThat(sentRequest.method()).isEqualTo("PUT");
    assertThat(sentRequest.headers()).isEmpty();
    assertThat(sentRequest.entity()).isEqualTo(entity);
    assertThat(sentRequest.target()).isEqualTo(url);
    assertThat(sentRequest.mediaType()).isEqualTo(MediaType.TEXT_PLAIN);
  }

  @Test
  void post() {
    Response expected = Response.status(Response.Status.NO_CONTENT).build();
    String url = "/post";
    DummyClient client = DummyClient.create().prepareResponse(url, expected).build();

    Entity<String> entity = Entity.text("entity");

    Response response = client.target(url).request().post(entity);

    assertThat(response).isEqualTo(expected);

    SentRequest sentRequest = client.getSentRequest(url);
    assertThat(sentRequest.method()).isEqualTo("POST");
    assertThat(sentRequest.headers()).isEmpty();
    assertThat(sentRequest.entity()).isEqualTo(entity);
    assertThat(sentRequest.target()).isEqualTo(url);
    assertThat(sentRequest.mediaType()).isNull();
  }

  @Test
  void postClassType() {
    String expected = "expected response";
    String url = "/post";
    DummyClient client = DummyClient.create().prepareResponse(url, expected).build();

    Entity<String> entity = Entity.text("entity");

    String response = client.target(url).request(MediaType.TEXT_PLAIN).post(entity, String.class);

    assertThat(response).isEqualTo(expected);

    SentRequest sentRequest = client.getSentRequest(url);
    assertThat(sentRequest.method()).isEqualTo("POST");
    assertThat(sentRequest.headers()).isEmpty();
    assertThat(sentRequest.entity()).isEqualTo(entity);
    assertThat(sentRequest.target()).isEqualTo(url);
    assertThat(sentRequest.mediaType()).isEqualTo(MediaType.TEXT_PLAIN);
  }

  @Test
  void postGenericType() {
    String expected = "expected response";
    String url = "/post";
    DummyClient client = DummyClient.create().prepareResponse(url, expected).build();

    Entity<String> entity = Entity.text("entity");

    String response =
        client.target(url).request(MediaType.TEXT_PLAIN).post(entity, new GenericType<>() {});

    assertThat(response).isEqualTo(expected);

    SentRequest sentRequest = client.getSentRequest(url);
    assertThat(sentRequest.method()).isEqualTo("POST");
    assertThat(sentRequest.headers()).isEmpty();
    assertThat(sentRequest.entity()).isEqualTo(entity);
    assertThat(sentRequest.target()).isEqualTo(url);
    assertThat(sentRequest.mediaType()).isEqualTo(MediaType.TEXT_PLAIN);
  }

  @Test
  void multipleMediaTypes() {
    DummyClient client = DummyClient.create().prepareResponse("", null).build();

    assertThatThrownBy(
            () -> client.target("").request(MediaType.APPLICATION_JSON, MediaType.TEXT_PLAIN))
        .isInstanceOf(UnsupportedOperationException.class)
        .hasMessage("Currently, only a single accepted response type is supported");
  }

  @Test
  void getSentRequestAfterMultipleRequests() {
    String url = "/path";
    DummyClient client = DummyClient.create().prepareResponse(url, 123).build();

    client.target(url).request().get(Integer.class);
    client.target(url).request().get(Integer.class);

    assertThatThrownBy(() -> client.getSentRequest(url))
        .isInstanceOf(IllegalStateException.class)
        .hasMessage("More than one request was sent to the specified URL");
  }

  @Test
  void prepareSameUrlTwice() {
    String url = "/url";
    DummyClient.Builder builder = DummyClient.create().prepareResponse(url, 123);
    assertThatThrownBy(() -> builder.prepareResponse(url, 456))
        .isInstanceOf(IllegalArgumentException.class)
        .hasMessage("Cannot prepare for the same URL twice");
  }
}
