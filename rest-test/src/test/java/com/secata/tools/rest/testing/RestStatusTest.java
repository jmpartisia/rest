package com.secata.tools.rest.testing;

import jakarta.ws.rs.RedirectionException;
import jakarta.ws.rs.WebApplicationException;
import jakarta.ws.rs.core.Response;
import jakarta.ws.rs.core.Response.Status;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

/** Test. */
public final class RestStatusTest {

  @Test
  public void compare() {
    Response response = Response.status(Status.OK).build();
    Assertions.assertThat(RestStatus.compareResponseWithStatus(response, Status.OK)).isTrue();
    Assertions.assertThat(RestStatus.compareResponseWithStatus(response, Status.CREATED)).isFalse();
    Assertions.assertThat(RestStatus.compareResponseWithStatus(response, null)).isFalse();
    Assertions.assertThat(RestStatus.compareResponseWithStatus(null, null)).isFalse();
    Assertions.assertThat(RestStatus.compareResponseWithStatus(null, Status.OK)).isFalse();
  }

  @Test
  public void result() {
    Response response = Response.status(Status.OK).build();
    Assertions.assertThat(response).is(RestStatus.result(Status.OK));
    Assertions.assertThat(response).isNot(RestStatus.result(Status.CREATED));

    // fields
    Assertions.assertThat(Response.status(Status.OK).build()).is(RestStatus.OK);
    Assertions.assertThat(Response.status(Status.CREATED).build()).is(RestStatus.CREATED);
    Assertions.assertThat(Response.status(Status.ACCEPTED).build()).is(RestStatus.ACCEPTED);
    Assertions.assertThat(Response.status(Status.NO_CONTENT).build()).is(RestStatus.NO_CONTENT);
    Assertions.assertThat(Response.status(Status.RESET_CONTENT).build())
        .is(RestStatus.RESET_CONTENT);
    Assertions.assertThat(Response.status(Status.PARTIAL_CONTENT).build())
        .is(RestStatus.PARTIAL_CONTENT);
  }

  @Test
  public void redirect() {
    RedirectionException exception = new RedirectionException(Status.FOUND, null);
    Assertions.assertThat(exception).is(RestStatus.redirect(Status.FOUND));
    Assertions.assertThat(exception).isNot(RestStatus.redirect(Status.SEE_OTHER));
    Assertions.assertThat(new Object()).isNot(RestStatus.redirect(Status.FOUND));
    Assertions.assertThat((RedirectionException) null).isNot(RestStatus.redirect(Status.FOUND));

    Assertions.assertThat(new RedirectionException(Status.MOVED_PERMANENTLY, null))
        .is(RestStatus.MOVED_PERMANENTLY);
    Assertions.assertThat(new RedirectionException(Status.FOUND, null)).is(RestStatus.FOUND);
    Assertions.assertThat(new RedirectionException(Status.SEE_OTHER, null))
        .is(RestStatus.SEE_OTHER);
    Assertions.assertThat(new RedirectionException(Status.NOT_MODIFIED, null))
        .is(RestStatus.NOT_MODIFIED);
    Assertions.assertThat(new RedirectionException(Status.USE_PROXY, null))
        .is(RestStatus.USE_PROXY);
    Assertions.assertThat(new RedirectionException(Status.TEMPORARY_REDIRECT, null))
        .is(RestStatus.TEMPORARY_REDIRECT);

    Response response = Response.status(Status.FOUND).build();
    Assertions.assertThat(response).is(RestStatus.redirect(Status.FOUND));
    Assertions.assertThat(response).isNot(RestStatus.redirect(Status.SEE_OTHER));
    Assertions.assertThat(new Object()).isNot(RestStatus.redirect(Status.FOUND));
    Assertions.assertThat((Response) null).isNot(RestStatus.redirect(Status.FOUND));

    Assertions.assertThat(Response.status(Status.MOVED_PERMANENTLY).build())
        .is(RestStatus.MOVED_PERMANENTLY);
    Assertions.assertThat(Response.status(Status.FOUND).build()).is(RestStatus.FOUND);
    Assertions.assertThat(Response.status(Status.SEE_OTHER).build()).is(RestStatus.SEE_OTHER);
    Assertions.assertThat(Response.status(Status.NOT_MODIFIED).build()).is(RestStatus.NOT_MODIFIED);
    Assertions.assertThat(Response.status(Status.USE_PROXY).build()).is(RestStatus.USE_PROXY);
    Assertions.assertThat(Response.status(Status.TEMPORARY_REDIRECT).build())
        .is(RestStatus.TEMPORARY_REDIRECT);
  }

  @Test
  public void error() {
    WebApplicationException exception = new WebApplicationException(Status.BAD_REQUEST);
    Assertions.assertThat(exception).is(RestStatus.error(Status.BAD_REQUEST));
    Assertions.assertThat(exception).isNot(RestStatus.error(Status.BAD_GATEWAY));
    Assertions.assertThat(new RuntimeException()).isNot(RestStatus.error(Status.BAD_REQUEST));
    Assertions.assertThat((WebApplicationException) null)
        .isNot(RestStatus.error(Status.BAD_REQUEST));

    // fields
    Assertions.assertThat(new WebApplicationException(Status.BAD_REQUEST))
        .is(RestStatus.BAD_REQUEST);
    Assertions.assertThat(new WebApplicationException(Status.UNAUTHORIZED))
        .is(RestStatus.UNAUTHORIZED);
    Assertions.assertThat(new WebApplicationException(Status.PAYMENT_REQUIRED))
        .is(RestStatus.PAYMENT_REQUIRED);
    Assertions.assertThat(new WebApplicationException(Status.FORBIDDEN)).is(RestStatus.FORBIDDEN);
    Assertions.assertThat(new WebApplicationException(Status.NOT_FOUND)).is(RestStatus.NOT_FOUND);
    Assertions.assertThat(new WebApplicationException(Status.METHOD_NOT_ALLOWED))
        .is(RestStatus.METHOD_NOT_ALLOWED);
    Assertions.assertThat(new WebApplicationException(Status.NOT_ACCEPTABLE))
        .is(RestStatus.NOT_ACCEPTABLE);
    Assertions.assertThat(new WebApplicationException(Status.PROXY_AUTHENTICATION_REQUIRED))
        .is(RestStatus.PROXY_AUTHENTICATION_REQUIRED);
    Assertions.assertThat(new WebApplicationException(Status.REQUEST_TIMEOUT))
        .is(RestStatus.REQUEST_TIMEOUT);
    Assertions.assertThat(new WebApplicationException(Status.CONFLICT)).is(RestStatus.CONFLICT);
    Assertions.assertThat(new WebApplicationException(Status.GONE)).is(RestStatus.GONE);
    Assertions.assertThat(new WebApplicationException(Status.LENGTH_REQUIRED))
        .is(RestStatus.LENGTH_REQUIRED);
    Assertions.assertThat(new WebApplicationException(Status.PRECONDITION_FAILED))
        .is(RestStatus.PRECONDITION_FAILED);
    Assertions.assertThat(new WebApplicationException(Status.REQUEST_ENTITY_TOO_LARGE))
        .is(RestStatus.REQUEST_ENTITY_TOO_LARGE);
    Assertions.assertThat(new WebApplicationException(Status.REQUEST_URI_TOO_LONG))
        .is(RestStatus.REQUEST_URI_TOO_LONG);
    Assertions.assertThat(new WebApplicationException(Status.UNSUPPORTED_MEDIA_TYPE))
        .is(RestStatus.UNSUPPORTED_MEDIA_TYPE);
    Assertions.assertThat(new WebApplicationException(Status.REQUESTED_RANGE_NOT_SATISFIABLE))
        .is(RestStatus.REQUESTED_RANGE_NOT_SATISFIABLE);
    Assertions.assertThat(new WebApplicationException(Status.EXPECTATION_FAILED))
        .is(RestStatus.EXPECTATION_FAILED);
    Assertions.assertThat(new WebApplicationException(Status.PRECONDITION_REQUIRED))
        .is(RestStatus.PRECONDITION_REQUIRED);
    Assertions.assertThat(new WebApplicationException(Status.TOO_MANY_REQUESTS))
        .is(RestStatus.TOO_MANY_REQUESTS);
    Assertions.assertThat(new WebApplicationException(Status.REQUEST_HEADER_FIELDS_TOO_LARGE))
        .is(RestStatus.REQUEST_HEADER_FIELDS_TOO_LARGE);
    Assertions.assertThat(new WebApplicationException(Status.INTERNAL_SERVER_ERROR))
        .is(RestStatus.INTERNAL_SERVER_ERROR);
    Assertions.assertThat(new WebApplicationException(Status.NOT_IMPLEMENTED))
        .is(RestStatus.NOT_IMPLEMENTED);
    Assertions.assertThat(new WebApplicationException(Status.BAD_GATEWAY))
        .is(RestStatus.BAD_GATEWAY);
    Assertions.assertThat(new WebApplicationException(Status.SERVICE_UNAVAILABLE))
        .is(RestStatus.SERVICE_UNAVAILABLE);
    Assertions.assertThat(new WebApplicationException(Status.GATEWAY_TIMEOUT))
        .is(RestStatus.GATEWAY_TIMEOUT);
    Assertions.assertThat(new WebApplicationException(Status.HTTP_VERSION_NOT_SUPPORTED))
        .is(RestStatus.HTTP_VERSION_NOT_SUPPORTED);
    Assertions.assertThat(new WebApplicationException(Status.NETWORK_AUTHENTICATION_REQUIRED))
        .is(RestStatus.NETWORK_AUTHENTICATION_REQUIRED);
  }
}
