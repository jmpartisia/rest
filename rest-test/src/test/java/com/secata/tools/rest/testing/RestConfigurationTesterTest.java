package com.secata.tools.rest.testing;

import com.secata.tools.rest.ClassBinder;
import com.secata.tools.rest.RootResource;
import com.secata.tools.thread.ShutdownHook;
import org.assertj.core.api.Assertions;
import org.glassfish.hk2.utilities.binding.AbstractBinder;
import org.glassfish.jersey.server.ResourceConfig;
import org.junit.jupiter.api.Test;

/** Test. */
public final class RestConfigurationTesterTest {

  @Test
  void expectClassesTooFew() {
    Assertions.assertThatThrownBy(
            () ->
                RestConfigurationTester.create(
                        new ShutdownHook(), new ResourceConfig(RootResource.class))
                    .expectClasses())
        .isInstanceOf(AssertionError.class)
        .hasMessageContaining("Actual and expected should have same size");
  }

  @Test
  void expectClassesTooMany() {
    Assertions.assertThatThrownBy(
            () ->
                RestConfigurationTester.create(new ShutdownHook(), new ResourceConfig())
                    .expectClasses(RootResource.class))
        .isInstanceOf(AssertionError.class)
        .hasMessageContaining("Actual and expected should have same size");
  }

  @Test
  void expectClassesNotMatching() {
    Assertions.assertThatThrownBy(
            () ->
                RestConfigurationTester.create(
                        new ShutdownHook(), new ResourceConfig(RootResource.class))
                    .expectClasses(String.class))
        .isInstanceOf(AssertionError.class)
        .hasMessageContaining("to contain exactly in any order");
  }

  @Test
  void expectClassesMatching() {
    Assertions.assertThat(
            RestConfigurationTester.create(
                    new ShutdownHook(), new ResourceConfig(RootResource.class))
                .expectClasses(RootResource.class))
        .isNotNull();
  }

  @Test
  void expectClassesNone() {
    Assertions.assertThat(
            RestConfigurationTester.create(new ShutdownHook(), new ResourceConfig())
                .expectClasses())
        .isNotNull();
  }

  private static final class CloseableA implements AutoCloseable {

    private final boolean failing;

    public CloseableA(boolean failing) {
      this.failing = failing;
    }

    @Override
    public void close() {
      if (failing) {
        throw new IllegalStateException("Failing for test");
      }
    }
  }

  private static final class CloseableB implements AutoCloseable {

    @Override
    public void close() {}
  }

  @Test
  void errorWhileClosingAutoCloseables() {
    ShutdownHook hook = new ShutdownHook();
    hook.register(new CloseableA(true));

    Assertions.assertThatThrownBy(() -> RestConfigurationTester.create(hook, new ResourceConfig()))
        .hasRootCauseInstanceOf(IllegalStateException.class)
        .hasRootCauseMessage("Failing for test");
  }

  @Test
  void expectAutoCloseablesTooFew() {
    ShutdownHook hook = new ShutdownHook();
    hook.register(new CloseableB());
    Assertions.assertThatThrownBy(
            () -> RestConfigurationTester.create(hook, new ResourceConfig()).expectAutoCloseables())
        .isInstanceOf(AssertionError.class)
        .hasMessageContaining("Actual and expected should have same size");
  }

  @Test
  void expectAutoCloseablesTooMany() {
    Assertions.assertThatThrownBy(
            () ->
                RestConfigurationTester.create(new ShutdownHook(), new ResourceConfig())
                    .expectAutoCloseables(CloseableB.class))
        .isInstanceOf(AssertionError.class)
        .hasMessageContaining("Actual and expected should have same size");
  }

  @Test
  void expectAutoCloseablesNotMatching() {
    ShutdownHook hook = new ShutdownHook();
    hook.register(new CloseableB());
    Assertions.assertThatThrownBy(
            () ->
                RestConfigurationTester.create(hook, new ResourceConfig())
                    .expectAutoCloseables(CloseableA.class))
        .isInstanceOf(AssertionError.class)
        .hasMessageContaining("to match given predicate but none did");
  }

  @Test
  void expectAutoCloseablesMatching() {
    ShutdownHook hook = new ShutdownHook();
    hook.register(new CloseableB());
    Assertions.assertThat(
            RestConfigurationTester.create(hook, new ResourceConfig())
                .expectAutoCloseables(CloseableB.class))
        .isNotNull();
  }

  @Test
  void expectAutoCloseablesNone() {
    Assertions.assertThat(
            RestConfigurationTester.create(new ShutdownHook(), new ResourceConfig())
                .expectAutoCloseables())
        .isNotNull();
  }

  private static final class DummyBinder extends AbstractBinder {

    @Override
    protected void configure() {
      // intentionally left empty
    }
  }

  @Test
  void expectBindersTooFew() {
    Assertions.assertThatThrownBy(
            () ->
                RestConfigurationTester.create(
                        new ShutdownHook(), new ResourceConfig().register(new DummyBinder()))
                    .expectBinders())
        .isInstanceOf(AssertionError.class)
        .hasMessageContaining("Actual and expected should have same size");
  }

  @Test
  void expectBindersTooMany() {
    Assertions.assertThatThrownBy(
            () ->
                RestConfigurationTester.create(new ShutdownHook(), new ResourceConfig())
                    .expectBinders(DummyBinder.class))
        .isInstanceOf(AssertionError.class)
        .hasMessageContaining("Actual and expected should have same size");
  }

  @Test
  void expectBindersNotMatching() {
    Assertions.assertThatThrownBy(
            () ->
                RestConfigurationTester.create(
                        new ShutdownHook(), new ResourceConfig().register(new DummyBinder()))
                    .expectBinders(ClassBinder.class))
        .isInstanceOf(AssertionError.class)
        .hasMessageContaining("to match given predicate but none did");
  }

  @Test
  void expectBindersMatching() {
    Assertions.assertThat(
            RestConfigurationTester.create(
                    new ShutdownHook(), new ResourceConfig().register(new DummyBinder()))
                .expectBinders(DummyBinder.class))
        .isNotNull();
  }

  @Test
  void expectBindersNone() {
    Assertions.assertThat(
            RestConfigurationTester.create(new ShutdownHook(), new ResourceConfig())
                .expectBinders())
        .isNotNull();
  }

  @Test
  void expectClassBindingsNotFound() {
    Assertions.assertThatThrownBy(
            () ->
                RestConfigurationTester.create(new ShutdownHook(), new ResourceConfig())
                    .expectClassBindings(String.class))
        .isInstanceOf(AssertionError.class)
        .hasMessageContaining("Actual and expected should have same size");
  }

  @Test
  void expectClassBindingsTooFew() {
    Assertions.assertThatThrownBy(
            () ->
                RestConfigurationTester.create(
                        new ShutdownHook(),
                        new ResourceConfig()
                            .register(
                                ClassBinder.create()
                                    .bind(Boolean.class, true)
                                    .bind(String.class, "foo")))
                    .expectClassBindings(Boolean.class))
        .isInstanceOf(AssertionError.class)
        .hasMessageContaining("Actual and expected should have same size");
  }

  @Test
  void expectClassBindingsTooMany() {
    Assertions.assertThatThrownBy(
            () ->
                RestConfigurationTester.create(
                        new ShutdownHook(), new ResourceConfig().register(ClassBinder.create()))
                    .expectClassBindings(String.class))
        .isInstanceOf(AssertionError.class)
        .hasMessageContaining("Actual and expected should have same size");
  }

  @Test
  void expectClassBindingsNotMatching() {
    Assertions.assertThatThrownBy(
            () ->
                RestConfigurationTester.create(
                        new ShutdownHook(),
                        new ResourceConfig()
                            .register(ClassBinder.create().bind(Boolean.class, true)))
                    .expectClassBindings(String.class))
        .isInstanceOf(AssertionError.class)
        .hasMessageContaining("to match given predicate but none did");
  }

  @Test
  void expectClassBindingsNull() {
    Assertions.assertThatThrownBy(
            () ->
                RestConfigurationTester.create(
                        new ShutdownHook(),
                        new ResourceConfig()
                            .register(ClassBinder.create().bind(Boolean.class, null)))
                    .expectClassBindings(Boolean.class))
        .isInstanceOf(AssertionError.class)
        .hasMessageContaining("Actual and expected should have same size");
  }

  @Test
  void expectClassBindingsNoneButFound() {
    Assertions.assertThatThrownBy(
            () ->
                RestConfigurationTester.create(
                        new ShutdownHook(),
                        new ResourceConfig()
                            .register(ClassBinder.create().bind(Boolean.class, true)))
                    .expectClassBindings(String.class))
        .isInstanceOf(AssertionError.class)
        .hasMessageContaining("to match given predicate but none did");
  }

  @Test
  void expectClassBindingsMatching() {
    Assertions.assertThat(
            RestConfigurationTester.create(
                    new ShutdownHook(),
                    new ResourceConfig().register(ClassBinder.create().bind(Boolean.class, true)))
                .expectClassBindings(Boolean.class))
        .isNotNull();
  }

  @Test
  void expectClassBindingsNone() {
    Assertions.assertThat(
            RestConfigurationTester.create(
                    new ShutdownHook(), new ResourceConfig().register(new DummyBinder()))
                .expectClassBindings())
        .isNotNull();
  }
}
