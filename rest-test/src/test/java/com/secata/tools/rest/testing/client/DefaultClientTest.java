package com.secata.tools.rest.testing.client;

import static org.assertj.core.api.Assertions.assertThatThrownBy;

import jakarta.ws.rs.client.Client;
import jakarta.ws.rs.core.Link;
import jakarta.ws.rs.core.UriBuilder;
import java.net.URI;
import org.assertj.core.api.ThrowableAssert;
import org.junit.jupiter.api.Test;

/** Test. */
final class DefaultClientTest {

  @SuppressWarnings("Convert2MethodRef")
  @Test
  void unsupportedOperation() {
    Client client = new DefaultClient() {};

    final Link link = Link.valueOf("</>; rel=\"http://example.net/foo\"");

    assertUnsupported(() -> client.close());
    assertUnsupported(() -> client.target("uri"));
    assertUnsupported(() -> client.target(URI.create("/")));
    assertUnsupported(() -> client.target(UriBuilder.newInstance()));
    assertUnsupported(() -> client.target(link));
    assertUnsupported(() -> client.invocation(link));
    assertUnsupported(() -> client.getSslContext());
    assertUnsupported(() -> client.getHostnameVerifier());
  }

  private void assertUnsupported(ThrowableAssert.ThrowingCallable shouldRaiseThrowable) {
    assertThatThrownBy(shouldRaiseThrowable).isInstanceOf(UnsupportedOperationException.class);
  }
}
