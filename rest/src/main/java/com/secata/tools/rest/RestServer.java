package com.secata.tools.rest;

import com.sun.net.httpserver.HttpServer;
import java.net.URI;
import org.glassfish.jersey.jdkhttp.JdkHttpServerFactory;
import org.glassfish.jersey.server.ResourceConfig;

/** Default embedded rest server configured to use Jersey. */
public final class RestServer extends AbstractRestServer<HttpServer> {

  /**
   * Jdk rest server with default port.
   *
   * @param config defines the functionality of the server
   */
  public RestServer(ResourceConfig config) {
    super(config);
  }

  /**
   * Jdk rest server with custom port and config.
   *
   * @param port of the server
   * @param config defines the functionality of the server
   */
  public RestServer(int port, ResourceConfig config) {
    super(port, config);
  }

  @Override
  HttpServer createServer(URI baseUri, ResourceConfig config) {
    return JdkHttpServerFactory.createHttpServer(baseUri, config);
  }

  @Override
  void closeServer(HttpServer httpServer, int delay) {
    httpServer.stop(delay);
  }
}
