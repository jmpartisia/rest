package com.secata.tools.rest;

/**
 * Abstract config with default port for a rest server.
 *
 * <p>It is encouraged that the implementation of this interface is as immutable as possible to
 * avoid the configuration changing during runtime of the server.
 *
 * <p>An easy way to do this is to use {@link Record} instead of {@link Class} for the
 * implementation. {@link java.util.List} and {@link java.util.Map} will not be immutable, this is a
 * compromise we have accepted for now.
 */
public interface RestConfig {

  /**
   * Get the port to start the rest server on, usually 8080.
   *
   * @return port for rest server
   */
  int port();

  /**
   * Specifies if a Grizzly container should be used for the rest server. This value is optional and
   * defaults to false.
   *
   * @return true if a Grizzly container should be used
   */
  default Boolean grizzlyServer() {
    return null;
  }
}
