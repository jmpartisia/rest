package com.secata.tools.rest.filter;

import jakarta.annotation.Priority;
import jakarta.inject.Inject;
import jakarta.ws.rs.container.ContainerRequestContext;
import jakarta.ws.rs.container.ContainerResponseContext;
import jakarta.ws.rs.container.ContainerResponseFilter;
import jakarta.ws.rs.container.ResourceInfo;
import jakarta.ws.rs.core.HttpHeaders;
import jakarta.ws.rs.core.MultivaluedMap;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

/** Removes caching in web server and client. */
@Priority(FilterPriorities.NO_CACHE_PRIORITY)
public final class NoCacheFilter implements ContainerResponseFilter {

  private final ResourceInfo resourceInfo;

  /**
   * Creates a new no cache filter based on the info - the resource method is used to choose
   * handling.
   *
   * @param resourceInfo the resource info
   */
  @Inject
  public NoCacheFilter(ResourceInfo resourceInfo) {
    this.resourceInfo = resourceInfo;
  }

  @Override
  public void filter(ContainerRequestContext request, ContainerResponseContext responseContext) {
    Method method = resourceInfo.getResourceMethod();
    if (method != null && method.isAnnotationPresent(CacheControl.class)) {
      MultivaluedMap<String, Object> headers = responseContext.getHeaders();
      headers.remove(HttpHeaders.CACHE_CONTROL);

      CacheControl annotation = method.getAnnotation(CacheControl.class);
      headers.putSingle(HttpHeaders.CACHE_CONTROL, buildCacheControlHeader(annotation));
    } else {
      responseContext.getHeaders().add(HttpHeaders.CACHE_CONTROL, "no-store, must-revalidate");
      responseContext.getHeaders().add(HttpHeaders.EXPIRES, 0);
    }
  }

  static String buildCacheControlHeader(CacheControl annotation) {
    List<String> list = new ArrayList<>();
    list.add(annotation.directive().asHeader());

    if (annotation.maxAge() != -1) {
      list.add(String.format("max-age=%d", annotation.maxAge()));
    }

    if (annotation.immutable()) {
      list.add("immutable");
    }

    return String.join(", ", list);
  }
}
