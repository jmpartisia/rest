package com.secata.tools.rest.filter;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.util.Locale;

/**
 * Customizes the Cache-Control header for resources. See:
 * https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Cache-Control
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
public @interface CacheControl {

  /**
   * The cache directive.
   *
   * @return the directive to use
   */
  Directive directive() default Directive.NO_CACHE;

  /**
   * The max age of the resource. -1 disables max-age.
   *
   * @return the max age of the request in seconds
   */
  int maxAge() default -1;

  /**
   * Sets the immutable flag.
   *
   * @return true if the resource is static
   */
  boolean immutable() default false;

  /** The directive part of the cache control header. */
  enum Directive {
    /** Public. */
    PUBLIC,
    /** Private. */
    PRIVATE,
    /** No cache. */
    NO_CACHE,
    /** No store. */
    NO_STORE;

    /**
     * Gets the enum as a http header.
     *
     * @return the http header
     */
    public String asHeader() {
      return name().toLowerCase(Locale.getDefault()).replace('_', '-');
    }
  }
}
