package com.secata.tools.rest.filter;

import jakarta.annotation.Priority;
import jakarta.ws.rs.container.ContainerRequestContext;
import jakarta.ws.rs.container.ContainerRequestFilter;
import jakarta.ws.rs.container.ContainerResponseContext;
import jakarta.ws.rs.container.ContainerResponseFilter;
import jakarta.ws.rs.core.UriInfo;
import java.util.function.LongSupplier;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;

/** Filter for logging all request on entering the server and on exiting. */
@Priority(FilterPriorities.LOGGING_FILTER_PRIORITY)
public final class LoggingFilter implements ContainerRequestFilter, ContainerResponseFilter {

  private static final ThreadLocal<DurationCompute> durationComputer = new ThreadLocal<>();
  private static final Logger logger = LoggerFactory.getLogger(LoggingFilter.class);

  @Override
  public void filter(ContainerRequestContext containerRequestContext) {
    durationComputer.set(new DurationCompute());
    String path = getPath(containerRequestContext);
    logger.info("Incoming: " + "method=" + containerRequestContext.getMethod() + ", path=" + path);
  }

  @Override
  public void filter(
      ContainerRequestContext requestContext, ContainerResponseContext responseContext) {
    DurationCompute durationCompute = getCurrent();
    long duration = -1;
    if (durationCompute != null) {
      duration = durationCompute.compute();
    }
    logger.info(
        "Outgoing: "
            + "method="
            + requestContext.getMethod()
            + ", path="
            + getPath(requestContext)
            + ", status="
            + responseContext.getStatus()
            + ", duration="
            + duration);
    LoggingFilter.durationComputer.remove();
    MDC.clear();
  }

  DurationCompute getCurrent() {
    return durationComputer.get();
  }

  String getPath(ContainerRequestContext containerRequestContext) {
    UriInfo uriInfo = containerRequestContext.getUriInfo();
    if (uriInfo != null) {
      return uriInfo.getPath();
    } else {
      return null;
    }
  }

  static final class DurationCompute {

    private final LongSupplier timeSupplier;
    private final long start;

    DurationCompute() {
      this(System::currentTimeMillis);
    }

    DurationCompute(LongSupplier timeSupplier) {
      this.timeSupplier = timeSupplier;
      this.start = timeSupplier.getAsLong();
    }

    long compute() {
      return timeSupplier.getAsLong() - start;
    }
  }
}
