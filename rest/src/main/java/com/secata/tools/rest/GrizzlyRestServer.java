package com.secata.tools.rest;

import java.net.URI;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import org.glassfish.grizzly.GrizzlyFuture;
import org.glassfish.grizzly.http.server.HttpServer;
import org.glassfish.jersey.grizzly2.httpserver.GrizzlyHttpServerFactory;
import org.glassfish.jersey.server.ResourceConfig;

/** Rest server configured to use Grizzly. */
public final class GrizzlyRestServer extends AbstractRestServer<HttpServer> {

  /**
   * Grizzly rest server with default port.
   *
   * @param config defines the functionality of the server
   */
  public GrizzlyRestServer(ResourceConfig config) {
    super(config);
  }

  /**
   * Grizzly rest server with custom port and config.
   *
   * @param port of the server
   * @param config defines the functionality of the server
   */
  public GrizzlyRestServer(int port, ResourceConfig config) {
    super(port, config);
  }

  @Override
  HttpServer createServer(URI baseUri, ResourceConfig config) {
    return GrizzlyHttpServerFactory.createHttpServer(baseUri, config);
  }

  @Override
  void closeServer(HttpServer httpServer, int delay) {
    GrizzlyFuture<HttpServer> shutdown = httpServer.shutdown();
    try {
      shutdown.get(delay, TimeUnit.SECONDS);
    } catch (TimeoutException | InterruptedException | ExecutionException e) {
      httpServer.shutdownNow();
    }
  }
}
