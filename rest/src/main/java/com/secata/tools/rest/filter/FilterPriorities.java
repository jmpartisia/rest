package com.secata.tools.rest.filter;

/** Used to defined constants used as priority for registered filters. */
public interface FilterPriorities {

  /** Priority for logging. */
  int LOGGING_FILTER_PRIORITY = 50;

  /** Priority for no cache. */
  int NO_CACHE_PRIORITY = 75;

  /** Priority for cors. */
  int CORS_FILTER_PRIORITY = 100;

  /** Priority for database. */
  int DATABASE_FILTER_PRIORITY = 1000;

  /** Priority for user access. */
  int USER_ACCESS_PRIORITY = 2000;
}
