package com.secata.tools.rest;

import com.google.errorprone.annotations.CanIgnoreReturnValue;
import java.util.ArrayList;
import java.util.List;
import org.glassfish.hk2.utilities.binding.AbstractBinder;

/** Binds multiple objects to classes for a server config. */
public final class ClassBinder extends AbstractBinder {

  private final List<RegisteredBinding<?>> registeredBindings;

  ClassBinder() {
    this.registeredBindings = new ArrayList<>();
  }

  /**
   * Creates a new class binder.
   *
   * @return the created object
   */
  public static ClassBinder create() {
    return new ClassBinder();
  }

  /**
   * Binds a class to a specific object, used in dependency injection.
   *
   * <p>This method is a no-op if the passed object is null.
   *
   * @param clazz the class
   * @param object the actual object that manages this class
   * @param <T> the type of object
   * @return the class binder
   */
  @CanIgnoreReturnValue
  public <T> ClassBinder bind(Class<T> clazz, T object) {
    if (object != null) {
      registeredBindings.add(new RegisteredBinding<>(clazz, object));
    }
    return this;
  }

  /**
   * Gets the registered bindings.
   *
   * @return registered bindings
   */
  public List<RegisteredBinding<?>> getRegisteredBindings() {
    return List.copyOf(registeredBindings);
  }

  @Override
  protected void configure() {
    for (RegisteredBinding<?> registeredBinding : registeredBindings) {
      handleRegisteredBinding(registeredBinding);
    }
  }

  private <T> void handleRegisteredBinding(RegisteredBinding<T> binding) {
    bind(binding.object).to(binding.clazz);
  }

  /**
   * Class representing a registered binding in a {@link ClassBinder}.
   *
   * @param <T> the type of the bound class
   */
  public static final class RegisteredBinding<T> {

    /** the class. */
    public final Class<T> clazz;

    /** the object. */
    public final T object;

    private RegisteredBinding(Class<T> clazz, T object) {
      this.clazz = clazz;
      this.object = object;
    }
  }
}
