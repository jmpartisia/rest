package com.secata.tools.rest;

import com.secata.tools.rest.filter.CorsFilter;
import com.secata.tools.rest.filter.LoggingFilter;
import com.secata.tools.rest.filter.NoCacheFilter;
import java.util.List;
import org.glassfish.jersey.jackson.JacksonFeature;

/** Default resources for RestServer. */
public final class RestResources {

  private RestResources() {}

  /** Default resources for rest. */
  public static final List<Class<?>> DEFAULT =
      List.of(
          RootResource.class,
          ObjectMapperProvider.class,
          JacksonFeature.class,
          LoggingFilter.class,
          CorsFilter.class,
          NoCacheFilter.class);
}
