package com.secata.tools.rest;

import org.glassfish.jersey.internal.inject.AbstractBinder;

/**
 * To use a version of {@link AliveCheck}, register an instance of this class to the {@link
 * org.glassfish.jersey.server.ResourceConfig}.
 */
public final class AliveCheckBinder extends AbstractBinder {

  private final AliveCheck aliveCheck;

  /**
   * Creates a new Alive checker binder.
   *
   * @param aliveCheck the alive check giving the status
   */
  public AliveCheckBinder(AliveCheck aliveCheck) {
    this.aliveCheck = aliveCheck;
  }

  @Override
  protected void configure() {
    bind(aliveCheck).to(AliveCheck.class);
  }
}
