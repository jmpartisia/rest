package com.secata.tools.rest;

import jakarta.annotation.security.PermitAll;
import jakarta.inject.Inject;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.core.Response;
import jakarta.ws.rs.core.Response.Status;
import java.util.Optional;
import org.glassfish.jersey.server.ResourceConfig;

/** Generic is alive service for any web application. */
@Path("/")
public final class RootResource {

  private final AliveCheck aliveCheck;

  /**
   * Default constructor. To set aliveCheck register an {@link AliveCheckBinder} to the {@link
   * ResourceConfig}.
   *
   * @param aliveCheck found in {@link ResourceConfig} if registered
   */
  @Inject
  public RootResource(Optional<AliveCheck> aliveCheck) {
    this.aliveCheck = aliveCheck.orElseGet(() -> () -> true);
  }

  /**
   * Checks the status of this resource, if available returns 204. An application can use arbitrary
   * complicated patterns to check if the server is alive - supplied at construction.
   *
   * @return {@link Status#NO_CONTENT} if available.
   */
  @GET
  @PermitAll
  public Response status() {
    if (aliveCheck.check()) {
      return Response.noContent().build();
    } else {
      return Response.status(Status.INTERNAL_SERVER_ERROR).build();
    }
  }
}
