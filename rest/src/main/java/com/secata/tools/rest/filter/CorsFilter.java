package com.secata.tools.rest.filter;

import jakarta.annotation.Priority;
import jakarta.ws.rs.container.ContainerRequestContext;
import jakarta.ws.rs.container.ContainerRequestFilter;
import jakarta.ws.rs.container.ContainerResponseContext;
import jakarta.ws.rs.container.ContainerResponseFilter;
import jakarta.ws.rs.container.PreMatching;
import jakarta.ws.rs.core.HttpHeaders;
import jakarta.ws.rs.core.Response;
import java.util.StringJoiner;

/** Allows request from different urls, needed for MPC to work. */
@PreMatching
@Priority(FilterPriorities.CORS_FILTER_PRIORITY)
public final class CorsFilter implements ContainerRequestFilter, ContainerResponseFilter {

  private static final String ALLOW_METHODS;
  private static final String ALLOW_HEADERS;
  private static final String EXPOSE_HEADERS;

  static {
    ALLOW_METHODS =
        new StringJoiner(", ").add("GET").add("POST").add("PUT").add("DELETE").toString();

    ALLOW_HEADERS =
        new StringJoiner(", ")
            .add(HttpHeaders.CONTENT_DISPOSITION)
            .add(HttpHeaders.CONTENT_TYPE)
            .add(HttpHeaders.ACCEPT)
            .add(HttpHeaders.AUTHORIZATION)
            .toString();

    EXPOSE_HEADERS = new StringJoiner(", ").add(HttpHeaders.CONTENT_DISPOSITION).toString();
  }

  @Override
  public void filter(ContainerRequestContext requestContext) {
    if (requestContext.getMethod().equalsIgnoreCase("OPTIONS")) {
      requestContext.abortWith(Response.ok().build());
    }
  }

  @Override
  public void filter(
      ContainerRequestContext requestContext, ContainerResponseContext responseContext) {
    responseContext.getHeaders().add("Access-Control-Allow-Origin", "*");
    responseContext.getHeaders().add("Access-Control-Allow-Methods", ALLOW_METHODS);
    responseContext.getHeaders().add("Access-Control-Allow-Headers", ALLOW_HEADERS);
    responseContext.getHeaders().add("Access-Control-Expose-Headers", EXPOSE_HEADERS);
  }
}
