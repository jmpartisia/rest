package com.secata.tools.rest;

import org.glassfish.jersey.server.ResourceConfig;

/**
 * Used to check aliveness of a server. To use this register an {@link AliveCheckBinder} to the
 * {@link ResourceConfig}.
 */
@FunctionalInterface
public interface AliveCheck {

  /**
   * Checks if system is alive.
   *
   * @return true if alive
   */
  boolean check();
}
