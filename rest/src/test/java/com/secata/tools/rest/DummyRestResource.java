package com.secata.tools.rest;

import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.core.MediaType;

/**
 * Dummy implementation for {@link DummyRestRunnerTest}.
 *
 * <p><b>Important:</b> Should not be used <i>anywhere</i> else.
 */
@Path("/hello")
public final class DummyRestResource {

  @GET
  @Produces(MediaType.APPLICATION_JSON)
  public String get() {
    return "Hello World!";
  }
}
