package com.secata.tools.rest.filter;

import jakarta.ws.rs.container.ContainerRequestContext;
import jakarta.ws.rs.container.ContainerResponseContext;
import jakarta.ws.rs.core.MultivaluedHashMap;
import jakarta.ws.rs.core.Response;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

/** Test. */
public final class CorsFilterTest {

  private CorsFilter corsFilter;
  private ContainerRequestContext requestMock;
  private ContainerResponseContext responseMock;
  private MultivaluedHashMap<String, Object> headerMap;

  /** Construct mocks for request and response. */
  @BeforeEach
  public void setUp() {
    corsFilter = new CorsFilter();

    requestMock = Mockito.mock(ContainerRequestContext.class);
    responseMock = Mockito.mock(ContainerResponseContext.class);

    headerMap = new MultivaluedHashMap<>();
    Mockito.when(responseMock.getHeaders()).thenReturn(headerMap);
  }

  @Test
  public void filterOptions() {
    Mockito.when(requestMock.getMethod()).thenReturn("OPTIONS");

    corsFilter.filter(requestMock);

    Mockito.verify(requestMock)
        .abortWith(
            Mockito.argThat(
                response -> response.getStatus() == Response.Status.OK.getStatusCode()));

    Assertions.assertThat(headerMap.isEmpty()).isEqualTo(true);

    corsFilter.filter(requestMock, responseMock);

    Assertions.assertThat(headerMap.size()).isEqualTo(4);
  }

  @Test
  public void filterOther() {
    Mockito.when(requestMock.getMethod()).thenReturn("GET");

    corsFilter.filter(requestMock);

    Mockito.verify(requestMock, Mockito.never()).abortWith(Mockito.any());

    Assertions.assertThat(headerMap.isEmpty()).isEqualTo(true);

    corsFilter.filter(requestMock, responseMock);

    Assertions.assertThat(headerMap.size()).isEqualTo(4);
  }
}
