package com.secata.tools.rest;

import com.secata.tools.rest.filter.NoCacheFilter;
import jakarta.ws.rs.client.ClientBuilder;
import jakarta.ws.rs.client.Invocation;
import jakarta.ws.rs.core.Response;
import jakarta.ws.rs.core.Response.Status;
import java.lang.reflect.Field;
import java.util.concurrent.ThreadLocalRandom;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicReference;
import java.util.function.Function;
import org.assertj.core.api.Assertions;
import org.glassfish.jersey.server.ResourceConfig;

/** Test. */
public abstract class AbstractServerTest {

  protected int port() {
    return ThreadLocalRandom.current().nextInt(20_000) + 1000;
  }

  protected <T extends AbstractRestServer<?>> void testStart(
      Function<ResourceConfig, T> createRestServer) throws InterruptedException {
    AtomicBoolean failed = new AtomicBoolean(false);
    AtomicReference<T> server = new AtomicReference<>();
    Thread thread =
        new Thread(
            () -> {
              try {
                ResourceConfig resourceConfig = new ResourceConfig();
                resourceConfig.register(new AliveCheckBinder(() -> true));
                server.set(createRestServer.apply(resourceConfig));
                server.get().start();
              } catch (Exception e) {
                failed.set(true);
                throw new RuntimeException("Error while doing things", e);
              }
            });
    thread.start();

    Invocation get =
        ClientBuilder.newClient().target("http://localhost:8080/").request().buildGet();

    long start = System.currentTimeMillis();

    boolean waiting = true;

    while (!failed.get() && waiting && (System.currentTimeMillis() - start) < 60_000) {
      try {
        Response invoke = get.invoke();
        waiting = invoke.getStatus() != Status.NO_CONTENT.getStatusCode();
      } catch (Exception e) {
        System.out.println("Server not up.");
      }

      //noinspection BusyWait
      Thread.sleep(25);
    }

    thread.interrupt();
    thread.join();

    Assertions.assertThat(failed).isFalse();
    Assertions.assertThat(waiting).isFalse();

    server.get().close();
  }

  protected void testStartStopCombinations(AbstractRestServer<?> server) {
    Assertions.assertThat(server.isRunning()).isFalse();
    server.close();
    Assertions.assertThat(server.isRunning()).isFalse();
    server.start();
    Assertions.assertThat(server.isRunning()).isTrue();
    server.start();
    Assertions.assertThat(server.isRunning()).isTrue();
    server.close();
    Assertions.assertThat(server.isRunning()).isFalse();
    server.close();
    Assertions.assertThat(server.isRunning()).isFalse();
    server.setDelay(5);
    server.start();
    Assertions.assertThat(server.isRunning()).isTrue();
    server.close();
    Assertions.assertThat(server.isRunning()).isFalse();
  }

  protected <T extends AbstractRestServer<?>> void testEmptyConfig(
      Function<ResourceConfig, T> createRestServer)
      throws NoSuchFieldException, IllegalAccessException {
    ResourceConfig initialConfig = new ResourceConfig();
    T server = createRestServer.apply(initialConfig);
    Field field = AbstractRestServer.class.getDeclaredField("config");
    field.setAccessible(true);
    ResourceConfig innerConfig = (ResourceConfig) field.get(server);

    // initial config has nothing
    Assertions.assertThat(RestResources.DEFAULT).isNotEmpty();
    for (Class<?> resource : RestResources.DEFAULT) {
      Assertions.assertThat(initialConfig.isRegistered(resource)).isFalse();
    }
    Assertions.assertThat(initialConfig.isRegistered(RootResource.class)).isFalse();

    // inner config has all
    for (Class<?> resource : RestResources.DEFAULT) {
      Assertions.assertThat(innerConfig.isRegistered(resource)).isTrue();
    }
    Assertions.assertThat(innerConfig.isRegistered(RootResource.class)).isTrue();
    server.close();
  }

  protected <T extends AbstractRestServer<?>> void testPopulatedConfig(
      Function<ResourceConfig, T> createRestServer)
      throws NoSuchFieldException, IllegalAccessException {
    ResourceConfig initialConfig = new ResourceConfig(NoCacheFilter.class, RootResource.class);
    initialConfig.register(NoCacheFilter.class);
    T server = createRestServer.apply(initialConfig);
    Field field = AbstractRestServer.class.getDeclaredField("config");
    field.setAccessible(true);
    ResourceConfig innerConfig = (ResourceConfig) field.get(server);

    // initial config only has no cache
    Assertions.assertThat(RestResources.DEFAULT).isNotEmpty();
    for (Class<?> resource : RestResources.DEFAULT) {
      if (resource.equals(NoCacheFilter.class) || resource.equals(RootResource.class)) {
        Assertions.assertThat(initialConfig.isRegistered(resource)).isTrue();
      } else {
        Assertions.assertThat(initialConfig.isRegistered(resource)).isFalse();
      }
    }

    // inner config has all
    for (Class<?> resource : RestResources.DEFAULT) {
      Assertions.assertThat(innerConfig.isRegistered(resource)).isTrue();
    }
    Assertions.assertThat(innerConfig.isRegistered(RootResource.class)).isTrue();
    server.close();
  }
}
