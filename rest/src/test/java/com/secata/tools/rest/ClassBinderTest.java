package com.secata.tools.rest;

import java.util.List;
import org.assertj.core.api.Assertions;
import org.glassfish.hk2.api.Descriptor;
import org.glassfish.hk2.api.DynamicConfiguration;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Mockito;

/** Test. */
public final class ClassBinderTest {

  @Test
  public void bind() {
    A a = new A();
    B b = new B();
    C c = new C();

    ClassBinder binder = ClassBinder.create().bind(A.class, a).bind(B.class, b);
    binder.bind(C.class, c);

    DynamicConfiguration configuration = Mockito.mock(DynamicConfiguration.class);
    binder.bind(configuration);
    ArgumentCaptor<Descriptor> arguments = ArgumentCaptor.forClass(Descriptor.class);
    Mockito.verify(configuration, Mockito.times(3)).bind(arguments.capture(), Mockito.eq(false));
    List<Descriptor> allValues = arguments.getAllValues();

    Assertions.assertThat(allValues).hasSize(3);

    Assertions.assertThat(allValues.get(0).getImplementation()).isEqualTo(A.class.getName());
    Assertions.assertThat(allValues.get(0).getAdvertisedContracts())
        .containsExactly(A.class.getName());

    Assertions.assertThat(allValues.get(1).getImplementation()).isEqualTo(B.class.getName());
    Assertions.assertThat(allValues.get(1).getAdvertisedContracts())
        .containsExactly(B.class.getName());

    Assertions.assertThat(allValues.get(2).getImplementation()).isEqualTo(C.class.getName());
    Assertions.assertThat(allValues.get(2).getAdvertisedContracts())
        .containsExactly(C.class.getName());

    Assertions.assertThat(binder.getRegisteredBindings()).hasSize(3);
    Assertions.assertThat(binder.getRegisteredBindings().get(0).clazz).isEqualTo(A.class);
  }

  @Test
  void tryBindingNull() {
    ClassBinder binder =
        ClassBinder.create().bind(A.class, new A()).bind(B.class, null).bind(C.class, new C());

    DynamicConfiguration configuration = Mockito.mock(DynamicConfiguration.class);
    binder.bind(configuration);
    ArgumentCaptor<Descriptor> arguments = ArgumentCaptor.forClass(Descriptor.class);
    Mockito.verify(configuration, Mockito.times(2)).bind(arguments.capture(), Mockito.eq(false));
    List<Descriptor> allValues = arguments.getAllValues();

    Assertions.assertThat(allValues).hasSize(2);
    Assertions.assertThat(allValues.get(0).getImplementation()).isEqualTo(A.class.getName());
    Assertions.assertThat(allValues.get(0).getAdvertisedContracts())
        .containsExactly(A.class.getName());

    Assertions.assertThat(allValues.get(1).getImplementation()).isEqualTo(C.class.getName());
    Assertions.assertThat(allValues.get(1).getAdvertisedContracts())
        .containsExactly(C.class.getName());

    Assertions.assertThat(binder.getRegisteredBindings()).hasSize(2);
    Assertions.assertThat(binder.getRegisteredBindings().get(0).clazz).isEqualTo(A.class);
  }

  static final class A {

    @Override
    public String toString() {
      return "A";
    }
  }

  static final class B {

    @Override
    public String toString() {
      return "B";
    }
  }

  static final class C {

    @Override
    public String toString() {
      return "C";
    }
  }
}
