package com.secata.tools.rest;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.Test;

/** Test. */
public final class RestConfigTest {

  @Test
  void configWithDefaultGrizzly() {
    DummyConfig dummyConfig = new DummyConfig(8080);
    assertThat(dummyConfig).isNotNull();
    assertThat(dummyConfig.grizzlyServer()).isNull();
  }

  record DummyConfig(int port) implements RestConfig {}
}
