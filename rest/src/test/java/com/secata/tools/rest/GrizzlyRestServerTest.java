package com.secata.tools.rest;

import jakarta.ws.rs.core.UriBuilder;
import java.net.URI;
import java.util.function.Function;
import org.assertj.core.api.Assertions;
import org.glassfish.grizzly.http.server.HttpServer;
import org.glassfish.jersey.server.ResourceConfig;
import org.junit.jupiter.api.Test;

/** Test. */
public final class GrizzlyRestServerTest extends AbstractServerTest {

  @Test
  public void start() throws InterruptedException {
    testStart(GrizzlyRestServer::new);
  }

  @Test
  public void startStopCombinations() {
    GrizzlyRestServer server = new GrizzlyRestServer(port(), new ResourceConfig());
    testStartStopCombinations(server);
  }

  @Test
  public void emptyConfig() throws NoSuchFieldException, IllegalAccessException {
    Function<ResourceConfig, GrizzlyRestServer> createServer =
        (ResourceConfig config) -> new GrizzlyRestServer(port(), config);
    testEmptyConfig(createServer);
  }

  @Test
  public void populatedConfig() throws NoSuchFieldException, IllegalAccessException {
    Function<ResourceConfig, GrizzlyRestServer> createServer =
        (ResourceConfig config) -> new GrizzlyRestServer(port(), config);
    testPopulatedConfig(createServer);
  }

  @Test
  void closeServerShutdownNow() {
    ResourceConfig config = new ResourceConfig();
    GrizzlyRestServer grizzlyRestServer = new GrizzlyRestServer(config);
    URI baseUri = UriBuilder.fromUri("http://0.0.0.0/").port(8080).build();
    HttpServer httpServer = grizzlyRestServer.createServer(baseUri, config);

    grizzlyRestServer.closeServer(httpServer, 0);

    Assertions.assertThat(httpServer.isStarted()).isFalse();
  }
}
