package com.secata.tools.rest;

/** Empty dummy config. */
public record DummyRestConfig(int port, Boolean grizzlyServer) implements RestConfig {}
