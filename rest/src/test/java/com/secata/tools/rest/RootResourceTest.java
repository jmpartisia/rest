package com.secata.tools.rest;

import jakarta.ws.rs.core.Response.Status;
import java.util.Optional;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

/** Test. */
public final class RootResourceTest {

  @Test
  public void defaultConstructor() {
    RootResource root = new RootResource(Optional.empty());
    Assertions.assertThat(root.status().getStatusInfo()).isEqualTo(Status.NO_CONTENT);
  }

  @Test
  public void error() {
    RootResource root = new RootResource(Optional.of(() -> false));
    Assertions.assertThat(root.status().getStatusInfo()).isEqualTo(Status.INTERNAL_SERVER_ERROR);
  }
}
