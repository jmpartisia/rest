package com.secata.tools.rest;

import java.util.function.Function;
import org.glassfish.jersey.server.ResourceConfig;
import org.junit.jupiter.api.Test;

/** Test. */
public final class RestServerTest extends AbstractServerTest {

  @Test
  public void start() throws InterruptedException {
    testStart(RestServer::new);
  }

  @Test
  public void startStopCombinations() {
    RestServer server = new RestServer(port(), new ResourceConfig());
    testStartStopCombinations(server);
  }

  @Test
  public void emptyConfig() throws NoSuchFieldException, IllegalAccessException {
    Function<ResourceConfig, RestServer> createServer =
        (ResourceConfig config) -> new RestServer(port(), config);
    testEmptyConfig(createServer);
  }

  @Test
  public void populatedConfig() throws NoSuchFieldException, IllegalAccessException {
    Function<ResourceConfig, RestServer> createServer =
        (ResourceConfig config) -> new RestServer(port(), config);
    testPopulatedConfig(createServer);
  }
}
