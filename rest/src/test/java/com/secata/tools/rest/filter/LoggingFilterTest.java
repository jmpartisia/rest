package com.secata.tools.rest.filter;

import static org.mockito.Mockito.when;

import jakarta.ws.rs.container.ContainerRequestContext;
import jakarta.ws.rs.container.ContainerResponseContext;
import jakarta.ws.rs.core.UriInfo;
import java.net.URI;
import java.util.Map;
import java.util.concurrent.atomic.AtomicLong;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.slf4j.MDC;

/** Test. */
public final class LoggingFilterTest {

  private final LoggingFilter loggingFilter = new LoggingFilter();
  private ContainerRequestContext request;
  private ContainerResponseContext response;

  /** Setup a logging filter and mocks for request and response. */
  @BeforeEach
  public void setUp() throws Exception {
    request = Mockito.mock(ContainerRequestContext.class);
    response = Mockito.mock(ContainerResponseContext.class);
    UriInfo uriInfo = Mockito.mock(UriInfo.class);
    when(uriInfo.getRequestUri()).thenReturn(new URI("/url"));
    when(request.getUriInfo()).thenReturn(uriInfo);
    when(response.getStatus()).thenReturn(200);
  }

  @Test
  public void filter() {
    loggingFilter.filter(request);
    Assertions.assertThat(loggingFilter.getCurrent()).isNotNull();
    checkOutbound();
    Assertions.assertThat(loggingFilter.getCurrent()).isNull();
  }

  @Test
  public void filterOnlyOutbound() {
    checkOutbound();
    Assertions.assertThat(loggingFilter.getCurrent()).isNull();
  }

  @Test
  public void nullUriInfo() {
    when(request.getUriInfo()).thenReturn(null);
    checkOutbound();
    Assertions.assertThat(loggingFilter.getCurrent()).isNull();
  }

  @Test
  public void getPath() {
    when(request.getUriInfo()).thenReturn(null);
    Assertions.assertThat(loggingFilter.getPath(request)).isNull();
    UriInfo uriInfo = Mockito.mock(UriInfo.class);
    when(uriInfo.getPath()).thenReturn("Test Path");
    when(request.getUriInfo()).thenReturn(uriInfo);
    Assertions.assertThat(loggingFilter.getPath(request)).isEqualTo("Test Path");
  }

  @Test
  public void computation() {
    AtomicLong currentTime = new AtomicLong(42);
    LoggingFilter.DurationCompute durationCompute =
        new LoggingFilter.DurationCompute(currentTime::get);
    currentTime.set(70);
    Assertions.assertThat(durationCompute.compute()).isEqualTo(70 - 42);
  }

  private void checkOutbound() {
    MDC.put("value", "foo");
    loggingFilter.filter(request, response);
    Assertions.assertThat(MDC.get("value")).isNull();

    Map<String, String> copyOfContextMap = MDC.getCopyOfContextMap();
    if (copyOfContextMap != null) {
      Assertions.assertThat(copyOfContextMap)
          .withFailMessage("Logging filter should clear the MDC when leaving.")
          .isEmpty();
    }
  }
}
